import React from 'react';

// 导入Ant Design 组件库相应组件
// import { button } from 'antd-mobile'

// 导入路由
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

// 导入组件
import Home from './pages/Home/index.js';  //导入首页组件,会选择当前文件夹下的index文件
import CityList from './pages/CityList/index.js';   //导入城市选择组件

import Map from './pages/Map/index.js';  //导入地图Map组件展示地图
import HouseDetail from './pages/HouseDetail/index.js';  //导入房屋详情页面组件,用于接收动态路由参数

import Login from './pages/Login/index.js';  //导入登录账号组件
import Registe from './pages/Registe/index.js';  //导入账号注册组件

import Rent from './pages/Rent/index.js';  //导入已发布房源列表页面组件
import RentAdd from './pages/Rent/Add/index.js';  //导入房源发布页面组件
import RentSearch from './pages/Rent/Search/index.js';  //导入关键词搜索小区信息页面组件

// 导入封装组件AuthRoute  路由访问控制组件
import AuthRoute from './components/AuthRoute/index.js';

function App() {
  return (
    <Router>
      <div className="App">

        {/* 挂载组件,配置路由规则 */}
        <Route path='/home' component={Home}></Route>

        {/* 匹配默认路由,重定向到home组件,render函数可以行内指定样式(类似定义组件),也可以指定一个路由 */}
        {/* Redirect组件是路由组件内部定义重定向的 */}
        {/* exact是Route的一个属性，认为其是一种严格匹配模式 */}
        <Route exact path='/' render={() => <Redirect to='/home' />}></Route>

        <Route path='/cityList' component={CityList}></Route>
        <Route path='/map' component={Map}></Route>

        {/* 涉及动态路由参数的匹配 */}
        <Route path='/detail/:id' component={HouseDetail}></Route>

        {/* 登录和注册组件的路由匹配 */}
        <Route path='/login' component={Login}></Route>
        <Route path='/registe' component={Registe}></Route>

        {/* AuthRoute组件为封装组件 登录后才能访问的组件,Rent组件为已发布房源列表页面组件的路由匹配 */}
        {/* exact是Route的一个属性，认为其是一种严格匹配模式 */}
        <AuthRoute exact path='/rent' component={Rent}></AuthRoute>
        {/* 登录后才能访问的组件,RentAdd组件为房源发布页面组件的路由匹配 */}
        <AuthRoute path='/rent/add' component={RentAdd}></AuthRoute>
        {/* 登录后才能访问的组件,RentSearch组件为关键词搜索小区信息页面组件的路由匹配 */}
        <AuthRoute path='/rent/search' component={RentSearch}></AuthRoute>
      </div>
    </Router>
  );
}

export default App;
