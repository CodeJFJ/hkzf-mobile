// 自己封装的工具类,获取好客租房项目token的键,自定义和设置本地存储的键名称一样
const TOKEN_NAME = 'login_Token';

// 获取 token
const getToken = () => localStorage.getItem(TOKEN_NAME);

// 设置 token
const setToken = value => localStorage.setItem(TOKEN_NAME, value);

// 删除 token
const removeToken = () => localStorage.removeItem(TOKEN_NAME);

// 是否登录（有权限）
const isAuth = () => !!getToken();

// 向外暴露工具类封装的属性
export { getToken, setToken, removeToken, isAuth };
