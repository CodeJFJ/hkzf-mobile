import axios from 'axios';

// 引入token工具类
import { getToken, removeToken } from './auth.js'

// 引入配置文件的环境变量baseURL的路径
import { BASE_URL } from './url.js';

// 创建axios的配置文件,里面配置了baseURL的路径
const config = {
  baseURL: BASE_URL
}

// axios的参考文档在github上,之前还不是很熟悉
const instance = axios.create(config);

/**
 * 功能处理-使用axios拦截器统一处理token
 * 
 * 1.在api.js 中，添加请求拦截器  (API.interceptors.request.user())
 * 2.获取到当前请求的接口路径（url）
 * 3.判断接口路径，是否是以/user 开头，并且不是登录或注册接口（只给需要的接口添加请求头）
 * 4.如果是，就添加请求头Authorization
 * 5.添加响应拦截器 (API.interceptors.response.use())
 * 6.判断返回值中的状态码
 * 7.如果是400，标示token超时或异常，直接移除token
 */

//  添加请求拦截器
instance.interceptors.request.use(config => {
  // 获取到当前请求的接口路径
  const { url } = config;
  // 判断请求url路径  判断以什么开头,不以等号判断,可能路径还有参数
  if (url.startsWith('/user') && !url.startsWith('/user/login') && !url.startsWith('/user/registered')) {
    // 添加请求头
    config.headers.Authorization = getToken();
  }
  return config;
})

// 添加响应拦截器
instance.interceptors.response.use(response => {
  // 获取响应的状态码
  const { status } = response.data;
  // 判断响应的状态码,是否为400,表示token超时异常,直接移除token
  if (status === 400) {
    // 说明token超时异常,直接移除token
    removeToken();
  }
  return response;
})

export { instance };  //这里导出了instance,修改组件里面的axios,可以简化请求路径