// 引入axios发送请求库
// import axios from 'axios';
// 现在创建了axios的配置文件,因此上面的导入axios就不需要了,需导入新的axios配置
import { instance } from './api.js';

const getCurrentCity = () => {

  // 获取本地存储定位信息
  // 注意:存储信息到本地存储中要把JSON字符串转化为JSON对象  '"local_City"'
  // const localCity = JSON.parse(localStorage.getItem('"local_City"'));
  const localCity = JSON.parse(localStorage.getItem('local_City'));

  //  本地存储中没有定位信息
  if (!localCity) {
    // 利用Promise函数来解决异步数据的返回
    return new Promise((resolve, reject) => {
      try {

        // 如果没有存储,需获取当前定位城市(地理信息)
        const myCity = new window.BMapGL.LocalCity();

        myCity.get(async res => {
          // 根据城市名称查询该城市信息
          let { data: infoRes } = await instance.get('/area/info', {
            params: {
              name: res.name
            }
          })
          // console.log(infoRes);  //打印输出查询出的城市信息
          if (infoRes.status != 200) {
            console.error(infoRes.description)
            return
          }

          // 本地存储中没有定位信息,将获取到的城市信息存储到本地存储中
          // 注意:存储信息到本地存储中要把JSON对象转化为JSON字符串
          localStorage.setItem('local_City', JSON.stringify(infoRes.body))

          // 返回获取的城市信息  成功的回调函数被调用
          resolve(infoRes.body);
        })

      } catch (error) {
        // 错误的回调函数被调用
        reject(error);
      }
    })
  }

  // 如果有城市信息,返回城市信息,返回一个城市的Promise对象即可 
  // 为了函数返回值的统一,也返回一个Promise
  // 这个地方resolve单词写成了reslove,导致我获取localCity对象的值,调用方法出错,我在网上查找改成这样'"local_City"',在大括号里面加上小括号,当时虽然解决了问题,但是在切换城市,返回页面又变成了城市上海,主要是上面的代码localCity的值为null,在首页的生命周期钩子函数里面获取城市,就直接进入当前代码的if分支语句里面了,终结是上面的单词写错了,上面的是方法参数写错单词没有事情,而这里是Promise对象API中的一个方法名
  return Promise.resolve(localCity);
}

// 向外暴露本地存储定位信息的方法,使用的是export 在其他组件引入时要知道方法的名称,以及要结构赋值,注意和export default的区别
export { getCurrentCity };

// 导入新的axios配置
export { instance } from './api.js';

// 环境变量中配置baseURL
export { BASE_URL } from './url.js';

// 导出本地存储token相关变量
export * from './auth.js';

// 导入本地存储中的当前定位城市信息
export { getCity, setCity } from './city.js';
