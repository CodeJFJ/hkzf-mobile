const TOKEN_NAME = 'local_City';

// 获取当前定位城市
const getCity = () => JSON.parse(localStorage.getItem(TOKEN_NAME)) || {};

// 设置当前定位城市
const setCity = value => localStorage.setItem(TOKEN_NAME, value);

// 向外暴露工具类封装的属性
export { getCity, setCity };