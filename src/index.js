import React from 'react';
import ReactDOM from 'react-dom';

// 导入组件库样式
import 'antd-mobile/dist/antd-mobile.css';

// 导入字体图标库样式
import './assets/fonts/iconfont.css';

// 放在组件库下面,全局样式覆盖组件库样式
import './index.css';

// 导入react-virtualized组件样式文件
import 'react-virtualized/styles.css';

// 注意:我们定义的样式要放在组件库样式的后面,这样才会生效,后面的样式会覆盖前面的样式
import App from './App';

// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );  //严格模式

ReactDOM.render(<App />, document.getElementById('root'));