// 解构赋值获得React.Component
import React, { Component, createRef } from 'react';

// 导入下面cssmodule样式文件
import styles from './index.module.css';

// 导入属性校验
import PropTypes from 'prop-types';

/**
 * 列表找房模块-吸顶功能
 * 
 * 实现思路:
 * 1.在页面滚动的时候，判断筛选栏上边是否还在可视区域内
 * 2.如果在，不需要吸顶 如果不在，就吸顶
 * 3.吸顶之后，元素脱标，房屋列表会突然往上调动筛选栏的高度，解决这个问题，我们用一个跟筛选栏相同的占位元素，在筛选栏脱标后，代替它撑起高度
 * 
 * 
 * 实现步骤:
 * 1.封装Sticky组件
 * 2.在HouseList页面中，导入Sticky组件
 * 3.使用Sticky组件包裹要实现吸顶功能的Filter组件
 * 4.在Sticky组件中，创建两个ref对象（placeholder，content），分别指向占位元素和内容元素
 * 5.在组件中，监听浏览器的scroll事件
 * 6.在scroll事件中，通过getBoundingClientRect()方法得到筛选栏占位元素当前位置
 * 7.判断top是否小于0（是否在可视区内）
 * 8.如果小于，就添加需要吸顶样式（fixed），同时设置占位元素高度
 * 9.否则，就移除吸顶样式，同时让占位元素高度为0
 */

class Sticky extends Component {

  //构造方法
  constructor(props) {
    super(props);
  }

  // 创建ref对象  ref创建对象是React.createRef(),上面进行了结构赋值,就可以在React里面操作dom了
  placeholder = createRef();
  content = createRef();

  //在组件中,监听浏览器的scroll事件
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  //组件卸载生命周期钩子函数
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  // scroll 事件的事件处理程序
  handleScroll = () => {
    //获取dom对象
    const placeholderEl = this.placeholder.current;  //当前dom对象
    const contentEl = this.content.current;

    // 解构赋值
    let { height } = this.props;

    // 通过getBoundingClientRect()方法得到筛选栏占位元素当前位置
    const { top } = placeholderEl.getBoundingClientRect();

    if (top < 0) {  //小于0表示不在可视区域

      // 吸顶
      contentEl.classList.add(styles.fixed);
      // 用一个跟筛选栏相同的占位元素，在筛选栏脱标后，代替它撑起高度
      placeholderEl.style.height = `${height}px`;
    } else {
      // 取消吸顶
      contentEl.classList.remove(styles.fixed);
      placeholderEl.style.height = '0px';
    }
  }

  render() {
    return (
      <div>
        {/* 占位元素 */}
        <div ref={this.placeholder} />

        {/* 内容元素 */}
        {/* this.props.children展示的就是被吸顶包裹的Filter组件 */}
        <div ref={this.content}>{this.props.children}</div>
      </div>
    )
  }

}

// 组件prop校验
Sticky.propTypes = {
  height: PropTypes.number.isRequired
}

export default Sticky;


/**
 * 封装组件原则:对变化点封装,变化点通过props动态设置
 */