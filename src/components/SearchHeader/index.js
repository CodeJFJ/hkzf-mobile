import React from 'react';

// 引入ant组件库
import { Flex } from 'antd-mobile';

//导入样式
import './index.scss';

// 导入 PropTypes校验的包
import PropTypes from 'prop-types';

// 导入withRouter高阶组件,包装我们的组件,就可以得到路由相关的信息了
import { withRouter } from 'react-router-dom';

// 封装组件,比较简单,推荐使用函数组件
function SearchHeader({ history, cityName, className }) {
  {/* 搜素框组件 */ }
  return (
    < Flex className={['search-box', className || ''].join(' ')} >
      <Flex className='search'>
        {/* 位置 */}
        <div className='location' onClick={() => { history.push('/cityList') }}>
          <span className='name'>{cityName}</span>
          <i className='iconfont icon-arrow'></i>
        </div>
        {/* 搜索表单 */}
        <div className='form' onClick={() => { history.push('/search') }}>
          <i className='iconfont icon-seach'></i>
          <span className='text'>请输入小区或地址</span>
        </div>
      </Flex>
      {/* 搜索框图标 */}
      < i className='iconfont icon-map' onClick={() => { history.push('/map') }}></i>
    </Flex >
  )
}

// 设置组件校验,要使用校验需要导入响应的包
SearchHeader.propTypes = {
  cityName: PropTypes.string.isRequired,
  className: PropTypes.string
}

// withRouter(SearchHeader)的返回值也是一个组件
// 高阶组件中的withRouter, 作用是将一个组件包裹进Route里面, 然后react-router的三个对象history, location, match就会被放进这个组件的props属性中
// 高阶组件会包装参数组件,返回一个新的组件,高阶组件会把组件的状态和属性传递给参数组件
export default withRouter(SearchHeader);

// 上面代码，导出封装好的组件,有样式,有功能,可以直接导入使用
