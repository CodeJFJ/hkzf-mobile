import React from 'react';

// 导入组件校验规则
import PropTypes from 'prop-types';

// 导入配置的环境变量
import { BASE_URL } from '../../utils/url.js';

// 导入组件样式
import styles from './index.module.css';

// 函数组件,是箭头函数
const NoHouse = ({ children }) => (
  <div className={styles.root}>
    <img
      className={styles.img}
      src={BASE_URL + '/img/not-found.png'}
      alt="暂无数据"
    />
    <p className={styles.msg}>{children}</p>
  </div>
)

NoHouse.propTypes = {
  // 开始是string字符串,现在改为node,表示任何可以渲染的内容
  children: PropTypes.node.isRequired
}

export default NoHouse;
