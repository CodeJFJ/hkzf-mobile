import React from 'react';

// // 导入antd-mobile库中组件
import { NavBar } from 'antd-mobile';

// 导入withRouter高阶组件
import { withRouter } from 'react-router-dom';

// 导入 PropTypes校验的包
import PropTypes from 'prop-types';

// 导入组件库的样式
// import './index.scss'  //因为导入下面cssmodule样式文件,生成唯一类名,就不需要该样式文件
// 导入下面cssmodule样式文件
import styles from './index.module.css';

// 方案一:封装组件,使用类组件,此方法还不行
/**
export default class NavHeader extends React.Component {
  render() {
    return <NavBar
      className='navbar'
      mode="light"
      // 字体图标不使用组件库
      icon={<i className='iconfont icon-back' />}
      onLeftClick={() => this.props.history.go(-1)}
    // 标题的内容是不定的,是从外界传递进来的
    >{this.props.children}</NavBar>
  }
}
*/


// 方案二: 封装组件, 使用函数组件(推荐)
function NavHeader({
  children,
  history,
  onLeftClick,
  className,
  rightContent
}) {

  // 若用户没有指定行为,就使用默认行为,返回上一项
  const defaultHandler = () => history.go(-1)

  return (
    <NavBar
      // join()方法把数组转化为字符串
      className={[styles.navbar, className || ''].join(' ')}
      mode="light"
      // 字体图标不使用组件库
      icon={<i className='iconfont icon-back' />}
      onLeftClick={onLeftClick || defaultHandler}
      //rightContent属性是ant组件库里面NavBar组件的一个配置项
      rightContent={rightContent}
    >
      {/*  标题的内容是不定的,是从外界传递进来的 */}
      {children}
    </NavBar>
  )
}

//  给封装的组件添加PropTypes校验
// 注意NavHeader.propTypes中的propTypes不是引入添加校验的变量名
NavHeader.propTypes = {
  children: PropTypes.string.isRequired,
  // 若用户指定了行为,就必须指定一个函数
  onLeftClick: PropTypes.func,
  className: PropTypes.string,
  rightContent: PropTypes.array
}

// withRouter(NavHeader)的返回值也是一个组件
// 高阶组件中的withRouter, 作用是将一个组件包裹进Route里面, 然后react-router的三个对象history, location, match就会被放进这个组件的props属性中
// 高阶组件会包装参数组件,返回一个新的组件,高阶组件会把组件的状态和属性传递给参数组件
export default withRouter(NavHeader)

// 这个封装的组件里面有很多的知识点是重点,高阶组件withRouter