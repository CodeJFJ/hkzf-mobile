import React from 'react';

// 导入路由
import { Route, Redirect } from 'react-router-dom';

// 导入
import { isAuth } from '../../utils/index.js';

/**
 * 封装AuthRoute鉴权路由组件
 *
 * 1.在components目录中创建AuthRoute/index.js 文件
 * 2.创建组件AuthRoute并导出
 * 3.在AuthRoute组件中返回Route组件（在Route基础上做了一层包装，用于实现自定义功能）
 * 4.给Route组件，添加render方法，指定改组件要渲染的内容（类似与component属性）
 * 5.在render方法中，调用isAuth() 判断是否登陆
 * 6.如果登陆了，就渲染当前组件（通过参数component获取到要渲染的组件，需要重命名）
 * 7.如果没有登陆，就重定向到登陆页面，并且指定登陆成功后腰跳转的页面路径
 * 8.将AuthRoute组件接收到的props原样传递给Route组件（保证与Route组件使用方式相同）
 * 9.使用AuthRoute组件配置路由规则，验证是否实现页面的登陆访问控制
 */

//  定义函数组件
const AuthRoute = ({ component: Component, ...rest }) => {

  return (
    <Route
      // 封装组件中的一系列属性,可变参数
      {...rest}
      render={(props) => {
        const isLogin = isAuth();

        // 判断是否登录
        if (isLogin) {
          // 已登录,将props传递给组件,组件中才能获取到路由相关的信息
          return <Component {...props} />
        } else {
          // 未登录
          return (
            <Redirect
              to={{
                // 未登录跳转到登录页面
                pathname: '/login',
                state: {
                  // 如果没有登陆，就重定向到登陆页面，并且指定登陆成功后要跳转的页面路径,并且将需要访问页面的信息传递给登录组件页面
                  from: props.location
                }
              }}
            />
          )
        }

      }}
    >
    </Route>
  )
}

export default AuthRoute;