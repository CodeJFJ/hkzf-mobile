import React from 'react';

// 引入axios发送请求库
// import axios from 'axios';
// 现在创建了axios的配置文件,因此上面的导入axios就不需要了,需导入新的axios配置
import { instance } from '../../utils/api.js';

// 导入配置的环境变量
import { BASE_URL } from '../../utils/url.js';

// 导入组件
import { Carousel, Flex, Grid, WingBlank } from 'antd-mobile';

// 导入导航菜单栏本地图片
import nav1 from '../../assets/images/nav-1.png';
import nav2 from '../../assets/images/nav-2.png';
import nav3 from '../../assets/images/nav-3.png';
import nav4 from '../../assets/images/nav-4.png';

// 导入首页的样式
import './index.scss';

// 导入当前城市信息
import { getCurrentCity } from '../../utils/index.js';

// 导入封装好的导航栏组件
import SearchHeader from '../../components/SearchHeader/index.js';

// 定义导航菜单项数据
const nav = [{
  id: 0,
  img: nav1,
  title: '整租',
  path: '/home/list'
}, {
  id: 1,
  img: nav2,
  title: '合租',
  path: '/home/list'
}, {
  id: 2,
  img: nav3,
  title: '地图找房',
  path: '/map'
}, {
  id: 3,
  img: nav4,
  title: '去出租',
  path: '/rent/add'
}]

// 获取地理位置信息
// navigator.geolocation.getCurrentPosition(position => {
//   console.log("当前地理位置信息", position);
// })

export default class Index extends React.Component {

  // 组件状态
  state = {
    // 图片保留状态state
    imgHeight: 212,
    // 轮播图状态
    swipers: [],
    // 轮播图是否加载完毕
    isSwiperLoaded: false,
    // 租房小组状态
    groups: [],
    // 最新资讯
    news: [],
    // 当前城市名称
    curCityName: '上海'
  }

  // 获取轮播图请求数据
  async swipers() {
    let { data: res } = await instance.get('/home/swiper')
    // console.log(res);
    if (res.status != 200) {
      console.error(res.description)
      return
    }
    this.setState({
      swipers: res.body,
      isSwiperLoaded: true
    })
  }

  // 获取租房小组数据  
  // 现在补充:axios里面get传参方式,一通过问号拼接,二通过params
  async getGroups() {
    let { data: res } = await instance.get('/home/groups', {
      params: {
        'area': 'AREA|88cff55c-aaa4-e2e0'
      }
    })
    // console.log(res);  //打印获取租房小组数据
    // 判断返回的状态是否成功
    if (res.status != 200) {
      console.error(res.description)
      return
    }
    // 获取数据设置租房小组状态
    this.setState({
      groups: res.body
    })
  }

  // 获取最新资讯数据
  async getNews() {
    let { data: res } = await instance.get('/home/news', {
      params: {
        'area': 'AREA|88cff55c-aaa4-e2e0'
      }
    })
    // 判断返回的状态是否成功
    if (res.status != 200) {
      console.error(res.description)
      return
    }
    // console.log(res);  //打印获取最新资讯数据
    // 获取数据设置最新资讯状态
    this.setState({
      news: res.body
    })
  }

  // 渲染轮播图逻辑代码
  renderSwipers() {
    /* 遍历状态里面的数据,创建对应的a标签和img标签 */
    return this.state.swipers.map(item => (
      <a
        key={item.id}
        href='http://www.itcast.cn'
        style={{
          display: 'inline-block', width: '100%', height: this.state.imgHeight
        }}
      >
        <img
          src={BASE_URL + item.imgSrc}
          alt=""
          style={{ width: '100%', verticalAlign: 'top', height: this.state.imgHeight }}
        />
      </a>
    ))
  }

  // 渲染导航菜单逻辑代码
  renderNavs() {
    return nav.map(item => (
      <Flex.Item key={item.id}
        onClick={() => { this.props.history.push(item.path) }}>
        <img src={item.img} alt='' />
        <h2>{item.title}</h2>
      </Flex.Item>)
    )
  }

  // 自定义布局方法代码重构(租房小组)
  renderGroups(item) {
    return (
      <Flex className='group-item' justify='around'>
        <div className='desc'>
          <p className='title'>{item.title}</p>
          <span className='info'>{item.desc}</span>
        </div>
        <img src={BASE_URL + item.imgSrc} alt='' />
      </Flex>
    )
  }

  // 渲染最新资讯逻辑代码
  renderNews() {
    return (
      this.state.news.map(item => (
        <div className='news-item' key={item.id}>
          <div className='imgwrap' >
            <img src={BASE_URL + item.imgSrc} alt="" className='img' />
          </div>
          {/* 组件属性direction为项目定位方向 */}
          <Flex className='content' direction='column' justify='between'>
            <h3 className='title'>{item.title}</h3>
            <Flex className='info' justify='between'>
              <span>{item.from}</span>
              <span>{item.date}</span>
            </Flex>
          </Flex>
        </div>)
      )
    )
  }

  // 生命周期钩子函数
  // 执行函数getCurrentCity用到await关键字,所以调动时要用async关键字
  async componentDidMount() {

    // 调用请求轮播图的方法
    this.swipers()

    // 调用请求租房小组的方法
    this.getGroups()

    // 调用请求最新资讯的方法
    this.getNews()

    // 通过IP定位获取当前城市名称
    // 方法一
    /**
    const curCity = new window.BMapGL.LocalCity();
    curCity.get(async res => {
      // console.log("当前城市信息", res);
      const { data: result } = await axios.get(`http://localhost:8080/area/info?name=${res.name}`)
      // console.log(result);  //打印输出通过IP获取地理位置信息
      // 使用第二种方法设置状态,this.setState方法参数不传递对象,而传递一个函数return出来一个对象
      if (result.status != 200) {
        console.error(res.description)
        return
      }
      this.setState(() => {
        return {
          curCityName: result.body.label
        }
      })
    })
     */

    // 方法二:调用函数(抽取方法一代码块封装函数,位置'../../utils/index.js')当前城市信息函数(类似Java里面的工具类)
    const currentCity = await getCurrentCity();
    // console.log("在首页打印", CurrentCity);
    this.setState(() => {
      return {
        curCityName: currentCity.label
      }
    })
  }

  render() {
    return <div className='index'>
      {/* 轮播图组件 */}
      <div className='swiper'>
        {this.state.isSwiperLoaded ? (<Carousel
          // 自动播放
          autoplay={true}
          // 无限循环
          infinite
          // 自动切换的时间
          autoplayInterval='2000'
        >
          {this.renderSwipers()}
        </Carousel>) : ('')}

        {/* 搜素框组件 */}
        {/* 下面代码注释了,搜索框组件已经封装为单独组件,可以复用封装组件 */}
        {/* 下面的代码只供展示,样式已经删除 */}
        {/* <Flex className='search-box'>
          <Flex className='search'>
            //位置
            <div className='location' onClick={() => { this.props.history.push('/cityList') }}>
              <span className='name'>{this.state.curCityName}</span>
              <i className='iconfont icon-arrow'></i>
            </div>
            //搜索表单
            <div className='form' onClick={() => { this.props.history.push('/search') }}>
              <i className='iconfont icon-seach'></i>
              <span className='text'>请输入小区或地址</span>
            </div>
          </Flex>
          //搜索框图标
          <i className='iconfont icon-map' onClick={() => { this.props.history.push('/map') }}></i>
        </Flex> */}

        {/* 使用封装组件 */}
        <SearchHeader cityName={this.state.curCityName}></SearchHeader>
      </div>

      {/* 导航菜单组件 */}
      <Flex className='nav'>
        {this.renderNavs()}
      </Flex>

      {/* 租房小组组件 */}
      <div className='group'>
        <h3 className='group-title'>
          租房小组<span className='more'>更多</span>
        </h3>
        <Grid data={this.state.groups}
          // 列数
          columnNum={2}
          // 是否强制为正方形
          square={false}
          // 是否有边框
          hasLine={false}
          // 自定义每个 grid 条目的创建函数
          renderItem={item => this.renderGroups(item)}
        />
      </div>

      {/* 最新资讯组件 */}
      <div className='news'>
        <h3 className='group-title'>最新资讯</h3>
        {/* 组件WingBlank设置两端留白 */}
        <WingBlank size="md">{this.renderNews()}</WingBlank>
      </div>
    </div>
  }
}