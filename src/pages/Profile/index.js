import React, { Component } from 'react';

import { Link } from 'react-router-dom';

// 导入ant组件库组件
import { Grid, Button, Modal } from 'antd-mobile';

// 导入axios配置信息
import { BASE_URL, instance, getToken, setToken, removeToken, isAuth } from '../../utils/index.js';

// 导入本地存储token的工具类
// import { getToken, setToken, removeToken, isAuth } from '../../utils/auth.js';  //因为在utils/index.js文件中导入了auth.js文件

// 导入组件样式
import styles from './index.module.css';

// 菜单数据
const menus = [
  { id: 1, name: '我的收藏', iconfont: 'icon-coll', to: '/favorate' },
  { id: 2, name: '我的出租', iconfont: 'icon-ind', to: '/rent' },
  { id: 3, name: '看房记录', iconfont: 'icon-record' },
  {
    id: 4,
    name: '成为房主',
    iconfont: 'icon-identity'
  },
  { id: 5, name: '个人资料', iconfont: 'icon-myinfo' },
  { id: 6, name: '联系我们', iconfont: 'icon-cust' }
]

// 默认头像
const DEFAULT_AVATAR = BASE_URL + '/img/profile/avatar.png'

/**
 * 判断用户是否登陆步骤
 * 
 * 1.在state中添加两个状态：isLogin（是否登录）和userInfo（用户信息）
 * 2.从utils中导入isAuth（登录状态）、getToken（获取token）
 * 3.创建方法getUserInfo，用户来获取个人资料
 * 4.在方法中，通过isLogin判断用户是否登录
 * 5.如果没有登录，则不发送请求，渲染未登录信息
 * 6.如果已登录，就根据接口发送请求，获取用户个人资料
 * 7.渲染个人资料数据
 */

export default class Profile extends Component {

  avatarEditor = React.createRef()

  // 组件状态
  state = {
    // 是否登录
    isLogin: isAuth(),  //isAuth()相当于调用了anth.js里面的函数,将函数的值赋值给组件的状态
    // 用户信息
    userInfo: {
      nickname: '',
      avatar: ''
    }
  }

  // 获取用户信息
  async getUserInfo() {
    // 是否当前为登录
    if (!this.state.isLogin) {
      return
    }

    // 发送请求,获取个人资料
    const { data: res } = await instance.get('/user', {
      // 请求头  注释原因:因为添加了路由请求拦截器
      // headers: {
      //   // 将token作为请求参数传递给后台,获取后台返回的个人资料
      //   authorization: getToken()
      // }
    })
    console.log(res);

    if (res.status == 200) {
      // 请求成功
      const { avatar, nickname } = res.body;
      // 获取的数据设置为组件状态
      this.setState(() => {
        return {
          userInfo: {
            avatar: BASE_URL + avatar,
            nickname
          }
        }
      })
    } else {
      // token失效的情况下,应当将isLogin的状态设置为false
      this.setState(() => {
        return {
          isLogin: false
        }
      })
    }

  }

  /**
   * 退出功能
   * 
   * 1.点击退出按钮，弹出对话框，提示是否确定退出
   * 2.给退出按钮绑定点击事件，创建方法logout作为事件处理函数
   * 3.导入Modal对话框组件（弹出模态框）
   * 4.在方法中，拷贝Modal组件文件高中确认对话框的示例代码
   * 5.修改对话框的文字提示
   * 6.在退出按钮的事件处理程序中，先调用退出接口（让服务器端退出），再移除本地token（本地退出）
   * 7.把登录状态isLogin设置为false
   * 8.清空用户状态对象
   */
  // 退出登录
  logout = () => {

    // Modal是ant组件库中对话框组件,alert为弹出对话框
    const alert = Modal.alert;

    alert('提示', '是否确定退出?', [
      { text: '取消' },
      {
        text: '退出', onPress: async () => {
          // 调用退出接口
          const { data: res } = await instance.post('/user/logout', null, {
            // 请求头   注释原因:因为添加了路由请求拦截器
            // headers: {
            //   // 将token作为请求参数传递给后台,获取后台返回的个人资料
            //   authorization: getToken()
            // }
          })

          // 补充,在状态码200时,再做出相应的业务逻辑
          if (res.status == 200) {
            // 退出登录,移除本地token
            removeToken();

            // 退出登录,重新还原组件的状态
            this.setState(() => {
              return {
                // 是否登录
                isLogin: false,
                // 用户信息
                userInfo: {
                  // 用户头像
                  avatar: '',
                  // 昵称用户名
                  nickname: ''
                }
              }
            })
          }

        }

      }
    ])

  }

  // 生命周期钩子函数
  componentDidMount() {
    // 在页面一加载时,记得调用
    this.getUserInfo();
  }

  render() {

    // 解构赋值
    const {
      isLogin,
      userInfo: { nickname, avatar }
    } = this.state;

    const { history } = this.props;

    return (
      <div className={styles.root}>
        {/* 个人信息 */}
        <div className={styles.title}>
          <img
            className={styles.bg}
            src={BASE_URL + '/img/profile/bg.png'}
            alt="背景图"
          />
          <div className={styles.info}>
            <div className={styles.myIcon}>
              <img className={styles.avatar} src={avatar || DEFAULT_AVATAR} alt="icon" />
            </div>
            <div className={styles.user}>
              <div className={styles.name}>{nickname || '游客'}</div>

              {/* 通过isLogin判断是否登录 */}
              {isLogin ? (
                // 渲染一个空标签,类似于vue里面的template标签
                <>
                  <div className={styles.auth}>
                    <span onClick={this.logout}>退出</span>
                  </div>
                  <div className={styles.edit}>
                    编辑个人资料
                  <span className={styles.arrow}>
                      <i className="iconfont icon-arrow" />
                    </span>
                  </div>
                </>
              ) : (
                  <div className={styles.edit}>
                    <Button
                      type="primary"
                      size="small"
                      inline
                      onClick={() => history.push('/login')}
                    >
                      去登录
                    </Button>
                  </div>
                )}
              {/* 登录后展示： */}
              {/* <>
                <div className={styles.auth}>
                  <span onClick={this.logout}>退出</span>
                </div>
                <div className={styles.edit}>
                  编辑个人资料
                  <span className={styles.arrow}>
                    <i className="iconfont icon-arrow" />
                  </span>
                </div>
              </> */}

              {/* 未登录展示： */}
              {/* <div className={styles.edit}>
                <Button
                  type="primary"
                  size="small"
                  inline
                  onClick={() => history.push('/login')}
                >
                  去登录
                </Button>
              </div> */}

            </div>
          </div>
        </div>

        {/* 九宫格菜单 */}
        <Grid
          data={menus}
          columnNum={3}
          // 九宫格是否带有边框
          hasLine={false}
          renderItem={item =>
            item.to ? (
              <Link to={item.to}>
                <div className={styles.menuItem}>
                  <i className={`iconfont ${item.iconfont}`} />
                  <span>{item.name}</span>
                </div>
              </Link>
            ) : (
                <div className={styles.menuItem}>
                  <i className={`iconfont ${item.iconfont}`} />
                  <span>{item.name}</span>
                </div>
              )
          }
        />

        {/* 加入我们 */}
        <div className={styles.ad}>
          <img src={BASE_URL + '/img/profile/join.png'} alt="" />
        </div>
      </div>
    )
  }
}
