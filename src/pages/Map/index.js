import React from 'react';

// 导入地图组件的样式
// import './index.scss'   //因为导入下面cssmodule样式文件,生成唯一类名,就不需要该样式文件
// 导入下面cssmodule样式文件
import styles from './index.module.css';

// 引入axios发送请求库
// import axios from 'axios';
// 现在创建了axios的配置文件,因此上面的导入axios就不需要了,需导入新的axios配置
import { instance } from '../../utils/api.js';

// 导入配置的环境变量
import { BASE_URL } from '../../utils/url.js';

// 导入封装NavBar组件
import NavHeader from '../../components/NavHeader/index.js';

// 导入渲染房屋列表的封装组件
import HouseItem from '../../components/HouseItem/index.js';

// 导入ant组件库组件
import { Toast } from 'antd-mobile';

// React官方组件,link导航到哪个路由
import { Link } from 'react-router-dom';

// 覆盖物的样式
const labelStyle = {
  cursor: 'pointer',
  border: '0px solid rgb(255,0,0)',
  padding: '0px',
  whiteSpace: 'nowrap',
  fontSize: '12px',
  color: 'rbg(255,255,255)',
  textAlign: 'center'
}

export default class Map extends React.Component {

  // 组件状态
  state = {
    housesList: [],  //房屋列表
    isShowList: false
  }

  // 初始化地图
  initMap() {
    // 获取本地存储中存储的城市列表切换城市信息
    const { label, value } = JSON.parse(localStorage.getItem('local_City'))
    // console.log(label, value);

    // 初始化地图实例
    // 注意: 在react脚手架中全局对象需要使用window来访问,否则,会造成ESlint校验错误
    const map = new window.BMapGL.Map("container");

    this.map = map;  //通过这样可以让其他方法引用该对象

    // 创建地址解析器实例
    const myGeo = new window.BMapGL.Geocoder()

    // 将地址解析结果显示在地图上,并调整地图视野
    // getPoint第一个参数为城市具体信息,第三个参数为城市名称

    let _this = this  //存在this指向问题,改变this指向,里面的函数没有改为箭头函数

    myGeo.getPoint(label, async function (point) {
      // point参数为城市的
      if (point) {
        // 初始化地图
        // 调用centerAndZoom()方法在地图中展示当前城市,并设置缩放级别为11
        map.centerAndZoom(point, 11);

        // 添加常用组件
        // anchor控制控件位置,BMAP_ANCHOR_BOTTOM_RIGHT表示控件定位于地图的右下角
        // { anchor: window.BMAP_ANCHOR_BOTTOM_RIGHT, type: window.BMAP_NAVIGATION_CONTROL_SMALL }
        map.addControl(new window.BMapGL.NavigationControl(
          {
            anchor: window.BMAP_ANCHOR_BOTTOM_RIGHT,
            type: window.BMAP_NAVIGATION_CONTROL_SMALL
          }));  //添加默认缩放平移控件
        map.addControl(new window.BMapGL.ScaleControl());  //添加比例尺

        //调用renderOverlays函数(自己封装的函数)
        _this.renderOverlays(value);  //存在this指向问题,推荐外面函数改为箭头函数,想学习一下this的使用

        /** 
         * 注释到105行左右

        // 获取房源数据
        let { data: res } = await axios.get(`http://localhost:8080/area/map?id=${value}`)
        console.log("房源数据：", res);

        // 遍历数据创建文本覆盖物,为每一条数据创建文本覆盖物
        res.body.forEach(item => {
          // 解构赋值拿到地理坐标点
          let { coord: { longitude, latitude }, label: areaName, count, value } = item;
          const areaPoint = new window.BMapGL.Point(longitude, latitude)

          // 创建一个labelText实例对象 
          const opts = {
            position: areaPoint,    // 指定文本标注所在的地理位置
            offset: new window.BMapGL.Size(-35, -35)    //设置文本偏移量
          }
          // 说明: 设置setContent后,第一个参数中设置的文本内容就失效了,因此,直接清空即可
          var labelText = new window.BMapGL.Label("", opts);  // 创建文本标注对象

          // 给labelText对象添加一个唯一标识
          labelText.id = value

          // 设置房源覆盖物的内容
          labelText.setContent(`
          <div class="${styles.bubble}">
            <p class="${styles.name}">${areaName}</p>
            <p>${count}套</p>
          </div>
          `)

          // 设置覆盖物样式
          labelText.setStyle(labelStyle);  //labelStyle是上面自定义好的样式

          // 添加覆盖物的单击事件
          labelText.addEventListener('click', () => {
            console.log('房源覆盖物事件被触发了', labelText.id);

            // 放大地图,以当前点击的覆盖物为中心放大地图
            // 第一个参数是坐标对象,第二个参数是放大级别
            map.centerAndZoom(areaPoint, 13);

            // 清除当前覆盖物信息,同时解决百度地图API的JS文件自身报错问题
            setTimeout(() => {
              map.clearOverlays()
            }, 0)

          })

          // 添加覆盖物到地图中
          map.addOverlay(labelText);
        })

        */

      }
    }, label);

    /**  注释代码地图确定位置写死了
    // // 设置中心点坐标
    const point = new window.BMapGL.Point(116.404, 39.915);
    // // 初始化地图
    map.centerAndZoom(point, 15);
     */


    //  给地图绑定移动时间
    //这个地方尽量用箭头函数,避免this执行问题,这个地方和上面的地方都用到匿名函数,一定要注意this指向问题,想学习一下this的使用
    map.addEventListener("movestart", function () {
      // _this.state.isShowList为true,即展示房源列表
      if (_this.state.isShowList) {
        // 在展示房源列表时,监听地图的DOM事件,若移动了地图,就让房源列表隐藏
        _this.setState(() => {
          return {
            isShowList: false
          }
        })
      }
    })

  }

  // 生命周期钩子函数
  componentDidMount() {
    // 调用初始化地图的方法
    this.initMap()
  }

  // 渲染覆盖物入口函数(封装代码)
  // 获取区域id参数,获取该区域下的房源数据
  // 获取房源类型以及下一级地图缩放级别
  async renderOverlays(id) {
    try {
      // 开启loading特效  第二个参数duration:自动关闭的延时，单位秒 第四个参数mask:是否显示透明蒙层，防止触摸穿透,默认为true
      Toast.loading('加载中...', 0, null, false)

      let { data: res } = await instance.get(`/area/map?id=${id}`)

      //若获取到数据,就关闭loading特效
      Toast.hide();

      // console.log("renderOverlays获取数据", id);
      // console.log(res);
      const data = res.body;  //该地区下区域房源信息
      //调用getTypeAndZoom()方法(该函数相当于计算属性,用于判断一下)
      const { nextZoom, type } = this.getTypeAndZoom();  //ES6解构赋值

      data.map(item => {
        this.createOverlays(item, nextZoom, type)  //创建覆盖物
      })

    } catch (e) {
      //若出现了异常,就关闭loading特效
      Toast.hide();
    }

  }

  //计算绘制的覆盖物类型和缩放级别(封装代码)
  // 区   11  范围10-12
  // 镇   13  范围12-14
  // 小区 15  范围14-16
  getTypeAndZoom() {
    // 获取当前地图缩放级别
    let zoom = this.map.getZoom()  //为什么可以获取map对象,是因为renderOverlays()中调用了getTypeAndZoom(),在initMap()中调用了renderOverlays()
    console.log("获取当前地图缩放级别", zoom);

    // 声明覆盖物类型type和缩放级别nextZoom变量
    let nextZoom, type;  //覆盖物类型type和缩放级别nextZoom

    if (zoom >= 10 && zoom < 12) {  //当前缩放级别是区
      nextZoom = 13;
      type = "circle";  // circle 表示绘制圆形的覆盖物，区
    } else if (zoom >= 12 && zoom < 14) {   //当前缩放级别是镇 
      nextZoom = 15;
      type = "circle";  // circle 表示绘制圆形的覆盖物，镇
    } else if (zoom >= 14 && zoom < 16) {  //当前缩放级别是小区
      type = "rect";  // circle 表示绘制矩形的覆盖物，小区
    }

    return { nextZoom, type }
  }

  //根据传入的类型，调用对应方法，创建覆盖物，到底是创建区镇的覆盖物还是小区覆盖物
  createOverlays(data, zoom, type) {
    // 解构赋值拿到地理坐标点
    let { coord: { longitude, latitude },
      label: areaName,
      count,
      value } = data;

    //创建坐标对象
    const areaPoint = new window.BMapGL.Point(longitude, latitude)

    // 判断需要渲染的是哪种类型
    if (type === 'circle') {
      // 区 或者 镇
      this.createCircle(areaPoint, areaName, count, value, zoom)
    } else if (type === 'rect') {
      // 小区
      this.createRect(areaPoint, areaName, count, value)
    }
  }

  // 复用之前的创建覆盖物的代码逻辑
  // 在覆盖物的单击事件中，调用 renderOverlays(id)方法，重新渲染该区域的房屋数据
  createCircle(point, name, count, id, zoom) {

    // 创建一个labelText实例对象 
    const opts = {
      position: point,    // 指定文本标注所在的地理位置
      offset: new window.BMapGL.Size(-35, -35)    //设置文本偏移量
    }
    // 说明: 设置setContent后,第一个参数中设置的文本内容就失效了,因此,直接清空即可
    var labelText = new window.BMapGL.Label("", opts);  // 创建文本标注对象

    // 给labelText对象添加一个唯一标识
    // labelText.id = id  //当前的唯一标识

    // 设置房源覆盖物的内容
    labelText.setContent(`
    <div class="${styles.bubble}">
      <p class="${styles.name}">${name}</p>
      <p>${count}套</p>
    </div>
    `)

    // 设置覆盖物样式
    labelText.setStyle(labelStyle);  //labelStyle是上面自定义好的样式

    // 添加覆盖物的单击事件
    labelText.addEventListener('click', () => {
      // console.log('房源覆盖物事件被触发了', labelText.id);

      //调用renderOverlays()方法,获取该区域下的房源数据
      this.renderOverlays(id);  //存在this指向问题,推荐外面函数改为箭头函数

      // 放大地图,以当前点击的覆盖物为中心放大地图
      // 第一个参数是坐标对象,第二个参数是放大级别
      this.map.centerAndZoom(point, zoom);

      // 清除当前覆盖物信息,同时解决百度地图API的JS文件自身报错问题
      setTimeout(() => {
        this.map.clearOverlays()
      }, 0)
    })

    // 添加覆盖物到地图中
    this.map.addOverlay(labelText);
  }

  // 创建Label、设置 样式、设置html内容，绑定事件
  // 在单击事件中，获取小区下的所有房源数据
  // 展示房源列表
  // 渲染获取到的房源列表
  createRect(point, name, count, id) {

    // 创建一个labelText实例对象 
    const opts = {
      position: point,    // 指定文本标注所在的地理位置
      offset: new window.BMapGL.Size(-50, -28)    //设置文本偏移量
    }
    // 说明: 设置setContent后,第一个参数中设置的文本内容就失效了,因此,直接清空即可
    var labelText = new window.BMapGL.Label("", opts);  // 创建文本标注对象

    // 给labelText对象添加一个唯一标识
    // labelText.id = id  //当前的唯一标识

    // 设置房源覆盖物的内容
    labelText.setContent(`
    <div class="${styles.rect}">
       <span class="${styles.housename}">${name}</span>
       <span class="${styles.housenum}">${count}套</span>
       <i class="${styles.arrow}"></i>
    </div>
    `)

    // 设置覆盖物样式
    labelText.setStyle(labelStyle);  //labelStyle是上面自定义好的样式

    // 添加覆盖物的单击事件
    labelText.addEventListener('click', event => {
      // console.log("小区被点击了...", event);

      // 使用地图的 panBy() 方法，移动地图到中间位置
      // 垂直位移：(window.innerHeight（屏幕高度）-330（房源列表高度）/2） - target.clientY（目标覆盖层的位置）
      // 水平位移：window.innerWidth（屏幕宽度）/2 - target.clientX

      // 获取当前被点击项
      const target = event.domEvent.changedTouches[0];
      this.map.panBy(window.innerWidth / 2 - target.clientX, (window.innerHeight - 330) / 2 - target.clientY);
      this.getHouseList(id);
    })

    // 添加覆盖物到地图中
    this.map.addOverlay(labelText);
  }

  // 获取小区房源数据
  async getHouseList(id) {
    try {
      // 开启loading特效
      Toast.loading('加载中...', 0, null, false)

      //请求小区房源数据
      let { data: res } = await instance.get(`/houses?cityId=${id}`);
      // console.log("小区房源数据", res);

      //若获取到数据,就关闭loading特效
      Toast.hide();

      this.setState(() => {
        return {
          housesList: res.body.list,
          isShowList: true  //开启展示房源列表
        }
      })

    } catch (e) {
      //若出现了异常,就关闭loading特效
      Toast.hide();
    }

  }

  // 渲染房屋列表
  renderHousesList() {

    // 使用渲染房屋列表的封装组件HouseItem
    return this.state.housesList.map(item => (
      <HouseItem
        key={item.houseCode}
        // 当前列表项的图片
        src={BASE_URL + item.houseImg}
        title={item.title}
        // 描述
        desc={item.desc}
        // 标签
        tags={item.tags}
        price={item.price}
      ></HouseItem>
    ))

    //注释一下代码,使用渲染房屋列表的封装组件来渲染房屋列表
    /** 
    return this.state.housesList.map(item => (
      <div className={styles.house} key={item.houseCode}>
        <div className={styles.imgWrap}>
          <img
            className={styles.img}
            src={BASE_URL + item.houseImg}
            alt=""
          />
        </div>
        <div className={styles.content}>
          <h3 className={styles.title}>{item.title}</h3>
          <div className={styles.desc}>{item.desc}</div>
          <div>
            {item.tags.map((tag, index) => {
              const tagClass = 'tag' + (index + 1)
              return (
                <span
                  className={[styles.tag, styles[tagClass]].join(' ')}
                  key={tag}
                >
                  {tag}
                </span>
              )
            })}
          </div>
          <div className={styles.price}>
            <span className={styles.priceNum}>{item.price}</span> 元/月
          </div>
        </div>
      </div>
    ))
    */

  }

  render() {
    return <div className={styles.map
    } >
      {/* 挂载调用封装好的NavBar组件 */}
      {/* 这里涉及到父组件向子组件通信,传递一个自定义函数 */}
      {/* console.log("使用NavBar组件返回按钮自定义事件") */}
      < NavHeader onLeftClick={() => this.props.history.go(-1)}> 地图找房</NavHeader >

      {/* 地图容器 */}
      <div id="container" className={styles.container}>
      </div>

      {/* 展示房源列表 */}
      {/* 添加 styles.show 展示房屋列表 */}
      <div className={[
        styles.houseList,
        this.state.isShowList ? styles.show : ''
      ].join(' ')}>
        <div className={styles.titleWrap}>
          <h1 className={styles.listTitle}>房屋列表</h1>
          <Link className={styles.titleMore} to="/home/list">
            更多房源</Link>
        </div>
        <div className={styles.houseItems}>
          {/* 房屋结构 */}
          {this.renderHousesList()}
        </div>
      </div>
    </div>
  }
}