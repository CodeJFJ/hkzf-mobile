import React, { Component } from 'react';

import { Link } from 'react-router-dom';

// 导入axios配置,以及环境变量
import { instance, BASE_URL } from '../../utils/index.js';

// 导入导航栏封装组件
import NavHeader from '../../components/NavHeader/index.js';
// 导入房屋列表每一项封装组件
import HouseItem from '../../components/HouseItem/index.js';
// 导入没有房屋信息的封装组件
import NoHouse from '../../components/NoHouse/index.js';

// 导入组件样式
import styles from './index.module.css';

export default class Rent extends Component {

  //组件状态
  state = {
    // 出租房屋列表
    list: []
  }

  // 生命周期钩子函数
  componentDidMount() {
    this.getHouseList()
  }

  // 获取已发布房源的列表数据
  async getHouseList() {
    const res = await instance.get('/user/houses')

    const { status, body } = res.data
    if (status === 200) {
      this.setState({
        list: body
      })
    } else {
      const { history, location } = this.props
      history.replace('/login', {
        from: location
      })
    }
  }

  renderHouseItem() {
    const { list } = this.state
    const { history } = this.props

    return list.map(item => {
      return (
        <HouseItem
          key={item.houseCode}
          onClick={() => history.push(`/detail/${item.houseCode}`)}
          src={BASE_URL + item.houseImg}
          title={item.title}
          desc={item.desc}
          tags={item.tags}
          price={item.price}
        />
      )
    })
  }

  renderRentList() {
    const { list } = this.state
    const hasHouses = list.length > 0

    if (!hasHouses) {
      return (
        <NoHouse>
          您还没有房源，
          <Link to="/rent/add" className={styles.link}>
            去发布房源
          </Link>
          吧~
        </NoHouse>
      )
    }

    return <div className={styles.houses}>{this.renderHouseItem()}</div>
  }

  render() {
    const { history } = this.props

    return (
      <div className={styles.root}>
        <NavHeader onLeftClick={() => history.go(-1)}>房屋管理</NavHeader>

        {this.renderRentList()}
      </div>
    )
  }
}
