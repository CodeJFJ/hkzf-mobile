import React, { Component } from 'react';

// 导入ant组件库组件
import { SearchBar } from 'antd-mobile';

// 导入utils/index.js里面向外暴露获取本地存储城市信息的属性
import { getCity, instance } from '../../../utils/index.js';

// 导入组件的样式
import styles from './index.module.css';

export default class Search extends Component {

  // 当前城市id
  cityId = getCity().value;
  timerId = null;

  // 组件样式
  state = {
    // 搜索框的值
    searchTxt: '',
    tipsList: []
  }

  /**
   * 传递小区信息给发布房源页面
   * 
   * 1.给搜索列表项添加点击事件
   * 2.在事件处理程序中，调用 history.replace() 方法跳转到发布房源页面
   * 3.将被点击的校区信息作为数据一起传递过去
   * 4.在发布房源页面，判断history.location.state 是否为空
   * 5.如果为空，不做任何处理
   * 6.如果不为空，则将小区信息存储到发布房源页面的状态中
   */
  onTipsClick = item => {
    // replace跳转不会形成history，不可返回到上一层。
    this.props.history.replace('/rent/add', {
      name: item.communityName,
      id: item.community
    })
  }

  // 渲染搜索结果列表
  renderTips = () => {
    const { tipsList } = this.state

    return tipsList.map(item => (
      <li
        key={item.community}
        className={styles.tip}
        // 注意:在点击事件中执行方法要传递参数,要写成一下方式,这样在点击时才触发函数
        onClick={() => this.onTipsClick(item)}
      >
        {item.communityName}
      </li>
    ))
  }

  // 监听搜索文本框组件文本框输入的值
  handleSearchTxt = value => {
    // 参数value为搜索文本框监听到的值
    this.setState({
      searchTxt: value
    })

    if (!value) {  //文本框输入的值为空
      this.setState({
        tipsList: []
      })
      return  //阻止代码向下运行
    }

    clearInterval(this.timerId);

    // 将变量存入到this中
    this.timerId = setTimeout(async () => {

      const { data: res } = await instance.get('/area/community', {
        params: {
          name: value,
          id: this.cityId
        }
      })
      console.log("获取小区关键词查询信息", res);

      this.setState(() => {
        return {
          tipsList: res.body
        }
      })

    }, 500)
  }

  /**
   * 关键词搜索小区信息
   * 
   * 1.给SearchBar组件，添加onChange配置项，获取文本框的值
   * 2.判断当前文本框的值是否为空
   * 3.如果为空，清空列表，然后return，不再发送请求
   * 4.如果不为空，使用API发送请求，获取小区数据
   * 5.使用定时器来延迟搜索，提升性能
   */

  render() {
    const { history } = this.props
    const { searchTxt } = this.state

    return (
      <div className={styles.root}>
        {/* 搜索框 */}
        <SearchBar
          placeholder="请输入小区或地址"
          value={searchTxt}
          showCancelButton={true}
          onCancel={() => history.replace('/rent/add')}
          onChange={this.handleSearchTxt}
        />

        {/* 搜索提示列表 */}
        <ul className={styles.tips}>{this.renderTips()}</ul>
      </div>
    )
  }
}
