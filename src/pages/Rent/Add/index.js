import React, { Component } from 'react';

// ant组件库
import {
  Flex,
  List,
  InputItem,
  Picker,
  ImagePicker,
  TextareaItem,
  Modal,
  // ant组件库中,提示组件
  Toast
} from 'antd-mobile';

// 导入导航栏封装组件
import NavHeader from '../../../components/NavHeader/index.js';
// 导入房屋配置封装组件
import HousePackge from '../../../components/HousePackage/index.js';

// 导入组件样式
import styles from './index.module.css';

// 导入axios配置
import { instance } from '../../../utils/api.js';

const alert = Modal.alert;

// 房屋类型
const roomTypeData = [
  { label: '一室', value: 'ROOM|d4a692e4-a177-37fd' },
  { label: '二室', value: 'ROOM|d1a00384-5801-d5cd' },
  { label: '三室', value: 'ROOM|20903ae0-c7bc-f2e2' },
  { label: '四室', value: 'ROOM|ce2a5daa-811d-2f49' },
  { label: '四室+', value: 'ROOM|2731c38c-5b19-ff7f' }
]

// 朝向：
const orientedData = [
  { label: '东', value: 'ORIEN|141b98bf-1ad0-11e3' },
  { label: '西', value: 'ORIEN|103fb3aa-e8b4-de0e' },
  { label: '南', value: 'ORIEN|61e99445-e95e-7f37' },
  { label: '北', value: 'ORIEN|caa6f80b-b764-c2df' },
  { label: '东南', value: 'ORIEN|dfb1b36b-e0d1-0977' },
  { label: '东北', value: 'ORIEN|67ac2205-7e0f-c057' },
  { label: '西南', value: 'ORIEN|2354e89e-3918-9cef' },
  { label: '西北', value: 'ORIEN|80795f1a-e32f-feb9' }
]

// 楼层
const floorData = [
  { label: '高楼层', value: 'FLOOR|1' },
  { label: '中楼层', value: 'FLOOR|2' },
  { label: '低楼层', value: 'FLOOR|3' }
]

export default class RentAdd extends Component {

  constructor(props) {
    super(props)

    // console.log(props);
    // 这里的state是路由携带的一些参数
    const { state } = props.location;
    const community = {
      name: '',
      id: ''
    }

    if (state) {
      // 有小区信息数据，存储到状态中
      community.name = state.name
      community.id = state.id
    }

    this.state = {
      // 临时图片地址
      tempSlides: [],

      // 小区的名称和id
      community,

      // 价格
      price: '',
      // 面积
      size: '',
      // 房屋类型
      roomType: '',
      // 楼层
      floor: '',
      // 朝向：
      oriented: '',
      // 房屋标题
      title: '',
      // 房屋图片
      houseImg: '',
      // 房屋配套：
      supporting: '',
      // 房屋描述
      description: ''
    }
  }

  // 取消编辑，返回上一页
  onCancel = () => {
    alert('提示', '放弃发布房源?', [
      {
        text: '放弃',
        onPress: async () => this.props.history.go(-1)
      },
      {
        text: '继续编辑'
      }
    ])
  }

  /**
   * 获取房源数据分析
   * 
   * 1.创建方法getValue作为三个组件的事件处理函数
   * 2.该方法接受两个参数：1. name 当前状态名；2. value 当前输入值或者选中值
   * 3.分别给 InputItem/TextareaItem/Picker 组件，添加onChange配置项
   * 4.分别调用 getValue 并传递 name 和 value 两个参数（注意：Picker组件选中值为数组，而接口需要字符串，所以，取索引号为 0 的值即可）
   */
  // 获取表单数据
  getValue = (name, value) => {
    // console.log(name, value);
    this.setState({
      // ES6属性名表达式
      [name]: value
    })
  }

  /**
   * 获取房屋配置数据
   * 
   * 1.给HousePackge 组件， 添加 onSelect 属性
   * 2.在onSelect 处理方法中，通过参数获取到当前选中项的值
   * 3.根据发布房源接口的参数说明，将获取到的数组类型的选中值，转化为字符串类型
   * 4.调用setState 更新状态
   */
  handleSupporting = selected => {
    this.setState({
      // 数组转化为字符串,拼接字符串
      supporting: selected.join('|')
    })
  }

  /**
   * 获取房屋图片
   * 
   * 1.给ImagePicker 组件添加 onChange 配置项
   * 2.通过onChange 的参数，获取到上传的图片，并且存储到tempSlides中
   */
  handleHouseImg = (files, type, index) => {
    // 参数files图片文件的数组  参数type操作类型,添加add或者删除remove(如果是删除,第三个参数则表示索引位置)
    console.log(files, type, index);
    this.setState({
      // tempSlides临时图片地址
      tempSlides: files
    })
  }

  /**
   * 上传房屋图片
   * 
   * 1.给提交按钮，绑定点击事件
   * 2.在事件处理函数中，判断是否有房屋图片
   * 3.如果没有，不做任何处理
   * 4.如果有，就创建FormData的示例对象（form）
   * 5.遍历tempSlides数组，分别将每一个图片图片对象，添加到form中（键为：file，根据接口文档获取）
   * 6.调用图片上传接口，传递form参数，并设置请求头 Content-Type 为 multipart/form-data
   * 7.通过接口返回值获取到图片路径
   */
  addHouse = async () => {
    // tempSlides图片临时状态
    const {
      // 图片临时地址
      tempSlides,
      title,
      description,
      oriented,
      supporting,
      price,
      roomType,
      size,
      floor,
      // 小区的名称和id
      community
    } = this.state;

    // 定义图片地址,用于存放从服务器获取的服务器图片地址,非上传组件的临时地址
    let houseImg = '';

    if (tempSlides.length > 0) {
      // 已经上传了图片
      const form = new FormData();
      tempSlides.forEach(item => form.append('file', item.file))

      const { data: res } = await instance.post('/houses/image', form, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      houseImg = res.body.join('|');
    }

    // 发布房源
    const { data: resp } = instance.post('/user/houses', {
      title,
      description,
      oriented,
      supporting,
      price,
      roomType,
      size,
      floor,
      // community小区的名称和id  存在一个bug,获取不到小区,因此就获取不到id
      community: community.id,
      houseImg
    })

    if (resp.status === 200) {
      // 发布成功
      Toast.info('发布成功', 1, null, false)
      this.props.history.push('/rent')
    } else {
      Toast.info('服务器偷懒了，请稍后再试~', 2, null, false)
    }

  }

  render() {
    const Item = List.Item
    const { history } = this.props
    const {
      community,
      price,
      size,
      roomType,
      floor,
      oriented,
      description,
      tempSlides,
      title
    } = this.state

    return (
      <div className={styles.root}>
        <NavHeader onLeftClick={this.onCancel}>发布房源</NavHeader>

        <List
          className={styles.header}
          renderHeader={() => '房源信息'}
          data-role="rent-list"
        >
          {/* 选择所在小区 */}
          <Item
            extra={community.name || '请输入小区名称'}
            arrow="horizontal"
            onClick={() => history.replace('/rent/search')}
          >
            小区名称
          </Item>

          {/* 相当于form表单里面的input元素 */}
          <InputItem
            placeholder="请输入租金/月"
            extra="￥/月"
            value={price}
            onChange={val => this.getValue('price', val)}
          >
            租&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;金
          </InputItem>

          <InputItem
            placeholder="请输入建筑面积"
            extra="㎡"
            value={size}
            onChange={val => this.getValue('size', val)}
          >
            建筑面积
          </InputItem>

          {/* data属性指定数据源 value属性指定选中的值 cols属性指定渲染几列 */}
          <Picker
            data={roomTypeData}
            value={[roomType]}
            cols={1}
            onChange={val => this.getValue('roomType', val[0])}
          >
            <Item arrow="horizontal">
              户&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;型
            </Item>
          </Picker>

          <Picker
            data={floorData}
            value={[floor]}
            cols={1}
            onChange={val => this.getValue('floor', val[0])}
          >
            <Item arrow="horizontal">所在楼层</Item>
          </Picker>

          <Picker
            data={orientedData}
            value={[oriented]}
            cols={1}
            onChange={val => this.getValue('oriented', val[0])}
          >
            <Item arrow="horizontal">
              朝&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;向
            </Item>
          </Picker>
        </List>

        <List
          className={styles.title}
          renderHeader={() => '房屋标题'}
          data-role="rent-list"
        >
          <InputItem
            placeholder="请输入标题（例如：整租 小区名 2室 5000元）"
            value={title}
            onChange={val => this.getValue('title', val)}
          />
        </List>

        <List
          className={styles.pics}
          renderHeader={() => '房屋图像'}
          data-role="rent-list"
        >
          {/* 图片选择器组件 */}
          <ImagePicker
            // tempSlides临时图片地址,组件的状态,用于回显上传的图片
            files={tempSlides}
            multiple={true}
            className={styles.imgpicker}
            onChange={this.handleHouseImg}
          />
        </List>

        <List
          className={styles.supporting}
          renderHeader={() => '房屋配置'}
          data-role="rent-list"
        >
          {/* HousePackge组件,组件添加select属性,组件就是可以选择的 */}
          <HousePackge select onSelect={this.handleSupporting} />
        </List>

        <List
          className={styles.desc}
          renderHeader={() => '房屋描述'}
          data-role="rent-list"
        >
          {/* 相当于form表单里面的textarea元素 */}
          <TextareaItem
            rows={5}
            placeholder="请输入房屋描述信息"
            autoHeight
            value={description}
            onChange={val => this.getValue('description', val)}
          />
        </List>

        <Flex className={styles.bottom}>
          <Flex.Item className={styles.cancel} onClick={this.onCancel}>
            取消
          </Flex.Item>
          <Flex.Item className={styles.confirm} onClick={this.addHouse}>
            提交
          </Flex.Item>
        </Flex>
      </div>
    )
  }
}
