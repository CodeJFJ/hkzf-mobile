import React, { Component } from 'react';

// 导入组件库  Modal提示框
import { Carousel, Flex, Modal, Toast } from 'antd-mobile';

// 导入封装组件
// 导入顶部导航栏封装组件
import NavHeader from '../../components/NavHeader/index.js';

// 导入房屋列表每一项封装组件
import HouseItem from '../../components/HouseItem/index.js';

// 导入房屋配套封装组件
import HousePackage from '../../components/HousePackage/index.js';

// 导入组件的样式
import styles from './index.module.css';

// 环境变量中配置baseURL
// import { BASE_URL } from '../../utils/url.js';

// 现在创建了axios的配置文件,因此上面的导入axios就不需要了,需导入新的axios配置
// import { instance } from '../../utils/api.js';

// 在utils/index.js里面导入了axios配置文件以及环境变量,因此上面的导入就注释了
import { instance, BASE_URL, isAuth } from '../../utils/index.js';

// 猜你喜欢
const recommendHouses = [
  {
    id: 1,
    src: BASE_URL + '/img/message/1.png',
    desc: '72.32㎡/南 北/低楼层',
    title: '安贞西里 3室1厅',
    price: 4500,
    tags: ['随时看房']
  },
  {
    id: 2,
    src: BASE_URL + '/img/message/2.png',
    desc: '83㎡/南/高楼层',
    title: '天居园 2室1厅',
    price: 7200,
    tags: ['近地铁']
  },
  {
    id: 3,
    src: BASE_URL + '/img/message/3.png',
    desc: '52㎡/西南/低楼层',
    title: '角门甲4号院 1室1厅',
    price: 4300,
    tags: ['集中供暖']
  }
]

// 房屋配套选项
// const supporting = [
//   '电视',
//   '冰箱',
//   '洗衣机',
//   '空调',
//   '热水器',
//   '沙发',
//   '衣柜',
//   '天然气'
// ]

// 百度地图
/**
 * 初始化地图实例
 * 
 * 注意: 在react脚手架中全局对象需要使用window来访问,否则,会造成ESlint校验错误
 * 
 * 新版本里面百度地图JS的SDK代码有所改变,创建百度地图对象,之前new window.BMap.Map('')创建对象,现在改为new window.BMapGL.Map('')
 */
const BMap = window.BMapGL;

const labelStyle = {
  position: 'absolute',
  zIndex: -7982820,
  backgroundColor: 'rgb(238, 93, 91)',
  color: 'rgb(255, 255, 255)',
  height: 25,
  padding: '5px 10px',
  lineHeight: '14px',
  borderRadius: 3,
  boxShadow: 'rgb(204, 204, 204) 2px 2px 2px',
  whiteSpace: 'nowrap',
  fontSize: 12,
  userSelect: 'none'
}

export default class HouseDetail extends Component {

  state = {
    // 是否被加载
    isLoading: false,

    // 房屋详情
    houseInfo: {
      // 房屋图片 6
      houseImg: [],
      // 标题 13
      title: '',
      // 标签  12
      tags: [],
      // 租金 8
      price: 0,
      // 房型 9
      roomType: '两室一厅',
      // 房屋面积 10
      size: 89,
      // 装修类型
      renovation: '精装',
      // 朝向 7
      oriented: [],
      // 楼层 4
      floor: '',
      // 小区名称 1
      community: '',
      // 地理位置 2
      coord: {
        latitude: '39.928033',
        longitude: '116.529466'
      },
      // 房屋配套  11
      supporting: [],
      // 房屋标识 5
      houseCode: '',
      // 房屋描述 3
      description: ''
    },

    // 房屋是否被收藏
    isFavorite: false
  }

  // 生命周期钩子函数
  async componentDidMount() {

    // 调用获取房屋数据
    this.getHouseDetail();

    // 调用检查房源是否被收藏
    this.checkFavorite();
  }

  /**
   * 收藏房源
   * 
   * 1.给收藏按钮绑定点击事件，创建方法handleFavorite作为事件处理程序
   * 2.调用isAuth方法，判断是否登陆
   * 3.如果未登录，则使用Modal.alert 提示用户是否去登陆
   * 4.如果点击取消，则不做任何操作
   * 5.如果点击去登陆，就跳转到登陆页面，同时传递state(登陆后，再回到房源收藏页面)
   * 6.根据isFavorite判断，当前房源是否收藏
   * 7.如果未收藏，就调用添加收藏接口，添加收藏
   * 8.如果收藏了，就调用删除接口，删除收藏
   */
  handleFavorite = async () => {
    // 调用导入的变量函数,判断是否登录
    const isLogin = isAuth();

    const { history, location, match } = this.props;

    // 未登录
    if (!isLogin) {

      // Modal是ant组件库中对话框组件,alert为弹出对话框
      const alert = Modal.alert;
      alert('提示', '登录后才能收藏房源，是否去登录?', [
        { text: '取消' },
        {
          text: '登录',
          onPress: () => history.push('/login', { from: location })
        }
      ])

      return
    }

    // 已登录
    const { isFavorite } = this.state;
    const { id } = match.params;  //获取参数id

    if (isFavorite) {
      // 已经收藏,再次点击就取消收藏
      const { data: res } = await instance.delete(`/user/favorites/${id}`);

      if (res.status === 200) {
        // 提示用户取消收藏
        Toast.info('已取消收藏', 1, null, false);
      } else {
        // token 超时
        Toast.info('登录超时，请重新登录', 2, null, false);
      }

      // 设置组件状态的收藏状态为false 不管是取消收藏成功,还是登录超时都应该让组件的登录状态置为false
      this.setState(() => {
        return {
          isFavorite: false
        }
      })

    } else {
      // 未收藏,再次点击就收藏
      const { data: res } = await instance.post(`/user/favorites/${id}`);

      if (res.status === 200) {
        // 提示用户收藏成功
        Toast.info('已收藏', 1, null, false)
        // 设置组件状态的收藏状态为false
        this.setState(() => {
          return {
            isFavorite: true
          }
        })
      } else {
        // token 超时
        Toast.info('登录超时，请重新登录', 2, null, false)
      }

    }

  }

  /**
   * 检查房源是否收藏
   * 
   * 1.在state中添加状态，isFavorite（表示是否收藏），默认值是false
   * 2.创建方法 checkFavorite，在进入房源详情页面时调用该方法
   * 3.先调用isAuth方法，来判断是否登陆
   * 4.如果未登录，直接return，不再检查是否收藏
   * 5.如果已登陆，从路由参数中，获取当前房屋id
   * 6.使用API调用接口，查询该房源是否收藏
   * 7.如果返回状态码为200，就更新isFavorite；否则，不做任何处理
   * 8.在页面结构中，通过状态isFavorite修改收藏按钮的文字和图片内容
   */
  async checkFavorite() {
    // 调用导入的变量函数,判断是否登录
    const isLogin = isAuth()

    if (!isLogin) {
      // 未登录
      return
    }

    // 获取路由参数id
    const { params: { id } } = this.props.match;  //这里面match都是路由对象提供的属性,例如有history

    const { data: res } = await instance.get(`/user/favorites/${id}`);

    // 请求成功
    if (res.status === 200) {
      this.setState({
        isFavorite: res.body.isFavorite
      })
    }

  }

  // 渲染轮播图结构
  renderSwipers() {

    const {
      houseInfo: { houseImg }
    } = this.state

    return houseImg.map(item => (
      <a
        key={item}
        href="http://itcast.cn"
        style={{
          display: 'inline-block',
          width: '100%',
          height: 252
        }}
      >
        <img
          src={BASE_URL + item}
          alt=""
          style={{ width: '100%', verticalAlign: 'top' }}
        />
      </a>
    ))
  }

  // 渲染地图
  renderMap(community, coord) {
    const { latitude, longitude } = coord;

    const map = new BMap.Map('map')
    const point = new BMap.Point(longitude, latitude)
    map.centerAndZoom(point, 17)

    const label = new BMap.Label('', {
      position: point,
      offset: new BMap.Size(0, -36)
    })

    label.setStyle(labelStyle)
    label.setContent(`
      <span>${community}</span>
      <div class="${styles.mapArrow}"></div>
    `)
    map.addOverlay(label)
  }

  // 渲染标签
  renderTags() {

    // 从状态中解构出标签
    const { houseInfo: { tags } } = this.state;

    return tags.map((item, index) => {

      let tagClass = '';

      if (index > 2) {  //若标签超过三个,后面的标签都展示第三个标签的样式
        tagClass = 'tag3';
      } else {
        tagClass = 'tag' + (index + 1);
      }

      return (
        <span key={item} className={[styles.tag, styles[tagClass]].join(' ')}>
          {item}
        </span>
      )
    })
  }

  // 获取房屋详情信息
  /**
   * 展示房屋详情
   * 
   * 1.在找房页面中，给每一个房源列表添加点击事件，在点击时跳转到房屋详情页面
   * 2.在单击事件中，获取到当前房屋id
   * 3.根据房屋详情的路由地址，调用history.push() 实现路由跳转
   * 4.封装getHouseDetail方法，在componentDidMount中调用该方法
   * 5.在方法中，通过路由参数获取到当前房屋id
   * 6.使用API发送请求，获取房屋数据，保存到state中
   * 7.使用房屋数据，渲染页面
   */
  async getHouseDetail() {

    // 开启loading,设置状态为true
    this.setState(() => {
      return {
        isLoading: true
      }
    })

    // 获取路由参数id
    const { params: { id } } = this.props.match;  //这里面match都是路由对象提供的属性,例如有history
    // <Route path='/detail/:id' component={...}></Route>

    // 向后台发送请求，获取房屋数据，保存到state中
    let { data: res } = await instance.get(`/houses/${id}`);
    console.log("获取房屋详情信息", res);

    // 获取了房屋详情信息后,再设置加载状态为false
    this.setState(() => {
      return {
        isLoading: false,
        // 给组件状态房屋详情数据赋值
        houseInfo: res.body
      }
    })

    // 从获取的房屋数据中解构出渲染地图所需要的地理坐标和小区名称
    const { community, coord } = res.body;
    // 渲染地图,调用渲染地图的方法
    this.renderMap(community, coord);

  }

  render() {

    const {
      isLoading,

      houseInfo: {
        community,  //小区名称
        title,  //房屋标题
        price,  //房屋价格
        roomType,  //房屋类型
        size,  //房屋面积
        floor,  //房屋所在楼层
        oriented,  //房屋朝向
        supporting,  //房屋配套
        description,  //房屋描述
      },

      isFavorite
    } = this.state;

    return (
      <div className={styles.root}>

        {/* 导航栏 */}
        <NavHeader
          // className={styles.navHeader}
          rightContent={[<i key="share" className="iconfont icon-share" />]}
        >
          {/* 渲染导航栏中房屋名称 */}
          {community}
        </NavHeader>

        {/* 轮播图 */}
        <div className={styles.slides}>
          {!isLoading ? (
            // 当数据加载完成之后,再渲染轮播图
            <Carousel
              autoplay
              infinite
              autoplayInterval={5000}
            >
              {/* 调用方法,渲染轮播图 */}
              {this.renderSwipers()}
            </Carousel>
          ) : ('')}
        </div>

        {/* 房屋基础信息 */}
        <div className={styles.info}>
          <h3 className={styles.infoTitle}>
            {title}
          </h3>
          <Flex className={styles.tags}>
            <Flex.Item>
              {/* 调用方法,渲染标签 */}
              {this.renderTags()}
            </Flex.Item>
          </Flex>

          <Flex className={styles.infoPrice}>
            <Flex.Item className={styles.infoPriceItem}>
              <div>
                {/* 渲染价格 */}
                {price}
                <span className={styles.month}>/月</span>
              </div>
              <div>租金</div>
            </Flex.Item>

            <Flex.Item className={styles.infoPriceItem}>
              {/* 渲染房屋类型 */}
              <div>{roomType}</div>
              <div>房型</div>
            </Flex.Item>

            <Flex.Item className={styles.infoPriceItem}>
              {/* 渲染房屋面积 */}
              <div>{size}平米</div>
              <div>面积</div>
            </Flex.Item>
          </Flex>

          <Flex className={styles.infoBasic} align="start">
            <Flex.Item>
              <div>
                <span className={styles.title}>装修：</span>
                精装
              </div>
              <div>
                <span className={styles.title}>楼层：</span>
                {/* 渲染房屋楼层 */}
                {floor}
              </div>
            </Flex.Item>

            <Flex.Item>
              <div>
                <span className={styles.title}>朝向：</span>
                {oriented.join('、')}
              </div>
              <div>
                <span className={styles.title}>类型：</span>
                普通住宅
              </div>
            </Flex.Item>
          </Flex>
        </div>

        {/* 地图位置 */}
        <div className={styles.map}>
          <div className={styles.mapTitle}>
            小区：
          <span>{community}</span>
          </div>
          {/* 呈现百度地图的容器 */}
          <div className={styles.mapContainer} id="map">
            地图
          </div>
        </div>

        {/* 房屋配套 */}
        <div className={styles.about}>
          <div className={styles.houseTitle}>房屋配套</div>

          {/* 判断是否有数据 */}
          {supporting.length === 0 ?
            (
              <div className="title-empty">暂无数据</div>
            ) :
            (<HousePackage
              list={supporting}
            />)
          }
        </div>

        {/* 房屋概况 */}
        <div className={styles.set}>
          <div className={styles.houseTitle}>房源概况</div>
          <div>
            <div className={styles.contact}>
              <div className={styles.user}>
                <img src={BASE_URL + '/img/avatar.png'} alt="头像" />
                <div className={styles.useInfo}>
                  <div>王女士</div>
                  <div className={styles.userAuth}>
                    <i className="iconfont icon-auth" />
                    已认证房主
                  </div>
                </div>
              </div>
              <span className={styles.userMsg}>发消息</span>
            </div>

            <div className={styles.descText}>
              {description || '暂无房屋描述'}
            </div>
          </div>
        </div>

        {/* 推荐 */}
        <div className={styles.recommend}>
          <div className={styles.houseTitle}>猜你喜欢</div>
          <div className={styles.items}>
            {recommendHouses.map(item => (
              <HouseItem {...item} key={item.id} />
            ))}
          </div>
        </div>

        {/* 底部收藏按钮 */}
        <Flex className={styles.fixedBottom}>
          <Flex.Item onClick={this.handleFavorite}>
            <img
              // 根据状态判断是否收藏,再显示收藏或者未收藏的样式
              src={BASE_URL + (isFavorite ? '/img/star.png' : '/img/unstar.png')}
              className={styles.favoriteImg}
              alt="收藏"
            />
            <span className={styles.favorite}>{isFavorite ? '已收藏' : '收藏'}</span>
          </Flex.Item>
          <Flex.Item>在线咨询</Flex.Item>
          <Flex.Item>
            <a href="tel:400-618-4000" className={styles.telephone}>
              电话预约
            </a>
          </Flex.Item>
        </Flex>
      </div>
    )
  }
}
