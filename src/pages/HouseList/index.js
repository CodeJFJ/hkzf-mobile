import React from 'react';

// 导入搜索导航栏的封装组件
import SearchHeader from '../../components/SearchHeader/index.js';

// 导入渲染房屋列表的封装组件
import HouseItem from '../../components/HouseItem/index.js';

// 导入吸顶组件
import Sticky from '../../components/Sticky/index.js';

// 导入在搜索没有房源数据时,显示没有房源数据的封装组件
import NoHouse from '../../components/NoHouse/index.js';

// 导入条件筛选栏组件
import Filter from './components/Filter/index.js';

// 导入ant组件库组件
import { Flex, Toast } from 'antd-mobile';

// 现在创建了axios的配置文件,因此上面的导入axios就不需要了,需导入新的axios配置
import { instance } from '../../utils/api.js';

// 导入react-virtualized库中组件
import { List, AutoSizer, WindowScroller, InfiniteLoader } from 'react-virtualized';

// 导入配置的环境变量
import { BASE_URL } from '../../utils/url.js';

// 导入下面cssmodule样式文件
import styles from './index.module.css';

// 导入获取本地存储定位信息的封装方法
import { getCurrentCity } from '../../utils/index.js'

// 获取当前城市的定位信息
// let { label, value } = JSON.parse(localStorage.getItem('local_City'));

/**
 * 在找房页面SearHeader组件基础上，调整结构(添加icon图标等)
 * 
 * 给SearchHeader组件传递className属性，来调整组件样式，让其适应找房页面效果
 */
export default class HomeList extends React.Component {

  // 组件状态
  state = {
    //存储房屋列表数据
    list: [],
    //存储房屋列表总条数
    count: 0,
    // 房源数据是否是加载中
    isLoading: false
  }

  /**
   * 进入页面时获取数据
   * 
   * 1.在componentDidMount钩子函数中，调用searchHouseList，来获取房屋列表数据
   * 2.给HouseList组件添加属性 filters，值为对象
   * 3.添加两个状态：list和count（存储房屋列表数据和总条数）
   * 4.将获取到的房屋数据，存储在state中
   */

  //初始化实例属性
  filters = {};
  label = '';  //对当前城市信息初始化,默认置为空,就不是null了
  value = '';

  // 生命周期钩子函数
  async componentDidMount() {

    /**
     * 注意一个细节:在组件外部的代码只会执行一次,在页面加载时执行,之后就不会执行,比如切换路由
     * 比如在切换城市时,城市在本地存储里面已经改变,但返回页面时并没有改变
     * 
     * 因此:我们不在组件外部通过本地存储获取城市信息,而采用在组件的内部通过调用方法获取城市信息
     */

    //  获取本地存储的城市信息
    const { label, value } = await getCurrentCity();

    // 将获取到的城市信息存储到this中,这样其他地方可以使用,不然在此钩子函数的局部作用域中获取,其他地方就不能使用到,可以学习到this的使用
    this.label = label;
    this.value = value;

    // 在页面加载就渲染房屋列表,此时没有在标题栏筛选数据,请求参数中filters为空
    this.searchHouseList();
  }

  //提供给Filter组件的方法,在组件里面调用,用于获取子组件(即Filter组件)传递给父组件(即该组件)的数据
  onFilter = filters => {
    console.log("获取子组件条件筛选数据", filters);

    // 之前浏览数据不在页面顶端,此时当切换查询条件时,重新加载页面,希望在页面顶端显示数据
    window.scrollTo(0, 0);

    // 通过参数接收filters数据(子组件传递给父组件),并存储到this中
    this.filters = filters;
    //调用方法查询获取房屋列表数据
    this.searchHouseList();
  }

  //用来获取房屋列表数据
  async searchHouseList() {

    // 当在请求房源数据时,让加载状态变为true
    this.setState(() => {
      return {
        isLoading: true
      }
    })

    //开启loading效果
    Toast.loading('加载中...', 0, null, false);

    //向后台请求数据
    let { data: res } = await instance.get("/houses", {
      params: {
        //获取请求城市的id,必选项
        cityId: this.value,
        //将从子组件获取的筛选数据展开
        ...this.filters,
        start: 1,
        end: 20
      }
    })
    console.log("传递到获取房源列表数据方法中", this.filters);
    console.log("根据条件查询房屋信息", res);

    //解构赋值出存储房屋列表和总条数
    let { list, count } = res.body;

    // 关闭loading
    Toast.hide();

    // 提示房源数
    // 解决了没有房源数据时,也弹窗提示的bug
    if (count != 0) {
      Toast.info(`共找到${count}套房源`, 2, null, false);
    }

    // 将获取到的数据赋值给组件状态
    this.setState(() => {
      return {
        list: list,
        count: count,
        // 当数据获取到了,再让加载状态变为默认false,添加这个业务逻辑,主要是为了在请求获取数据时,不展示没有房源数据提示的封装组件NoHouse组件
        isLoading: false
      }
    })
  }

  /**
   * 使用List组件渲染数据
   * 
   * 1.封装HouseItem组件，实现map和HouseListuemian中，房屋列表项的复用
   * 2.使用HouseItem组件改造Map组件的房屋列表项
   * 3.使用react-virtualized的List组件渲染房屋列表（参考CityList组件的使用）
   */
  // 渲染房屋列表中每一条数据的方法
  renderHouseList = ({
    key,
    index,  //索引
    style  // 重点属性：一定要给每一个行数添加该样式
  }) => {

    const { list } = this.state;
    //当前这一行房屋信息
    const house = list[index];

    // 判断house是否存在,若不存在就渲染一个loading效果,存在再渲染HouseItem组件
    if (!house) {
      return (
        <div key={key} style={style}>
          <p className={styles.loading}></p>
        </div>
      )
    }

    // 使用渲染房屋列表数据的封装组件
    // 其中key和style属性必须要传递,是react-virtualized库中List组件要求的
    return (
      <HouseItem
        key={key}

        // 给房屋列表中每一项添加点击事件
        onClick={() => { this.props.history.push(`/detail/${house.houseCode}`) }}

        // style属性必须传递,
        style={style}
        // 当前列表项的图片
        src={BASE_URL + house.houseImg}
        // 房屋列表标题
        title={house.title}
        // 描述
        desc={house.desc}
        // 标签
        tags={house.tags}
        price={house.price}
      >
      </HouseItem>
    )
  }

  // 判断每一行数据是否加载完毕
  isRowLoaded = ({ index }) => {
    return !!this.state.list[index];
  };

  /**
   * 加载更多房屋列表数据
   * 
   * 1.在loadMoreRows方法中，根据起始索引和结束索引，发送请求，获取更多房屋数据
   * 2.获取到最新的数据后，与当前list中的数据合并，再更新state，并调用Promise的resolve
   * 3.在renderHouseList方法中，判断house是否存在
   * 4.不存在的时候，就渲染一个loading元素
   * 5.存在的时候，再渲染HouseItem组件
   */

  // 用来获取更多房屋列表数据
  // 注意，该方法的返回值是一个 Promise 对象，并且，这个对象应该在数据加载完成时，来调用 resolve让 Promise对象的状态变为已完成
  loadMoreRows = ({ startIndex, stopIndex }) => {
    // 参数startIndex表示起始加载索引,stopIndex表示结束加载索引,默认是加载10条
    return new Promise(resolve => {

      instance.get("/houses", {
        //获取请求城市的id,必选项
        cityId: this.value,
        //将子组件获取的筛选数据展开
        ...this.filters,
        start: startIndex,
        end: stopIndex
      }).then(res => {
        console.log("获取更多房屋列表数据", res);

        this.setState(() => {
          return {
            // 展开运算符,获取到最新的数据后，与当前list中的数据合并
            list: [...this.state.list, ...res.data.body.list]
          }
        })

        // 数据加载完毕时,调用resolve方法即可
        resolve()
      })
    })
  }

  // 渲染列表数据
  renderList = () => {

    // 结构赋值
    const { count, isLoading } = this.state;

    /**
     * 若没有房源数据时,显示没有房源提示信息(即调用一个封装组件,显示提示信息)
     * 
     * 在数据加载完后,再进行对count的判断
     * 解决方案: 如果数据加载中,则不展示NoHouse组件,而在数据加载完后判断数据为0再展示NoHouse组件
     * 
     * 涉及思想:在数据的加载前后设置改变isLoading的状态,就可以避免在数据记载过程中显示没有数据提示的封装组件NoHouse组件
     */
    if (count == 0 && !isLoading) {
      // 组件的子元素作为参数传递到封装组件的内部
      return <NoHouse>没有找到房源,请您换一个搜索条件吧...</NoHouse>
    }

    return (
      //  高阶组件 
      < InfiniteLoader
        // 表示每一行数据是否加载完毕
        isRowLoaded={this.isRowLoaded}
        // 加载更多数据的方法,在需要更多的数据的时候,会调用这个方法
        loadMoreRows={this.loadMoreRows}
        // 总条数
        rowCount={count}
      >
        {({ onRowsRendered, registerChild }) => (
          // 高阶组件
          < WindowScroller >
            {({ height, isScrolling, scrollTop }) => (
              // 高阶组件   注意,在js代码里面的注释不能使用组件模板里面的注释,就是上面的注释
              <AutoSizer>
                {({ width }) => (
                  <List
                    // 
                    onRowsRendered={onRowsRendered}
                    // 
                    ref={registerChild}
                    //让List组件的高度是页面视口的高度,让List组件跟随页面滚动
                    autoHeight
                    //组件的宽度,由AutoSizer高阶组件来为List组件提供width
                    width={width}
                    //组件的高度,由WindowScroller组件提供,即页面视口的高度
                    height={height}
                    // List列表项总条目数
                    rowCount={count}
                    // 每行的高度
                    rowHeight={125}
                    // 获取当前列表渲染房屋列表数据信息
                    rowRenderer={this.renderHouseList}
                    // 表示是否滚动,用于覆盖List组件自身的滚动
                    isScrolling={isScrolling}
                    // 页面滚动的距离同步给List组件滚动的距离
                    scrollTop={scrollTop}
                  ></List>
                )}
              </AutoSizer>
            )}
          </WindowScroller>
        )}
      </InfiniteLoader>
    )
  }

  /**
   * 获取房屋数据
   * 
   * 1.将筛选条件数据filters传递给父组件HouseList
   * 2.HouseList组件中，创建方法onFilter，通过参数接收filters数据，并存储到this中
   * 3.创建方法searchHouseList（用来获取房屋列表数据）
   * 4.根据接口，获取当前定位城市id参数
   * 5.将筛选条件数据与分页数据合并后，作为借口的参数，发送请求，获取房屋数据
   */
  render() {

    return <div>
      {/* flex布局 */}
      <Flex className={styles.header}>
        {/* 下面的属性是字体图标 */}
        <i className="iconfont icon-back" onClick={() => { this.props.history.go(-1) }}></i>

        {/* cityName城市名称是动态获取的,应当基于一个默认值,在获取到城市追加给默认值 */}
        <SearchHeader cityName={this.label} className={styles.searchHeader}></SearchHeader>
      </Flex>

      {/* 条件筛选栏 */}
      {/* 要求条件筛选栏组件有吸顶效果,让吸顶组件Sticky包裹要有吸顶效果的组件Filter */}
      <Sticky height={40}>
        {/* 在吸顶Sticky封装组件的子节点中传递Filter组件,然后在Sticky组件的内部拿到传递进来的组件,有没有类似vue的插槽 */}
        <Filter onFilter={this.onFilter}></Filter>
      </Sticky>

      {/* 渲染房屋列表 */}
      {/* 
      1.滚动房屋列表时候，动态加载更多房屋数据
      2.使用`InfiniteLoader` 组件，来实现无限滚动列表，从而加载更多房屋数据
      3.根据 `InfiniteLoader` 文档示例，在项目中使用组件
      */}
      <div className={styles.houseItems}>{this.renderList()}</div>
    </div >
  }
}


/**
 * 使用 WindowScroller 跟随页面滚动
 *
 * 1.默认:List组件只让组件自身出现滚动条，无法让整个页面滚动，也就无法实现标题吸顶功能
 * 2.解决方式:使用`WindowScroller`高阶组件，让List组件跟随页面滚动（为List组件提供状态，同时还需要设置List组件的autoHeight属性）
 * 3.注意:`WindowScroller`高阶组件只能提供height，无法提供width
 * 4.解决方案:在WindowScroller组件中使用AutoSizer高阶组件来为List组件提供width
 */