import React from 'react'

// 导入ant组件库
import { Flex } from 'antd-mobile'

// 导入样式
import styles from './index.module.css'

// 条件筛选栏标题数组
const titleList = [
  { title: '区域', type: 'area' },
  { title: '方式', type: 'mode' },
  { title: '租金', type: 'price' },
  { title: '筛选', type: 'more' }
]

/**
 * 1.通过props接受，高亮状态对象 titleSelectedStatus(父组件传递到子组件)
 * 2.遍历titleList数组，渲染标题列表
 * 3.判断高亮对象中当前标题是否高亮，如果是，添加高亮类
 * 4.给标题项绑定单击事件，在事件中调用父组件传过来的方法 onClick
 * 5.将当前标题type，通过onClick的参数，传递给父组件
 * 6.父组件中接受到当前type，修改改标题的选中状态为true
 */

export default function FilterTitle({ titleSelectedStatus, onClick }) {

  return (
    <Flex align="center" className={styles.root}>
      {
        titleList.map(item => {
          // 判断当前项titleSelectedStatus[item.type]是否选中
          const isSelected = titleSelectedStatus[item.type];

          // 思路:子组件调用父组件传递来的函数,即调用回调函数
          return <Flex.Item key={item.type} onClick={() => { onClick(item.type) }}>
            {/* 选中类名： selected */}
            {/* 若选中就添加选中类名,就增加样式 */}
            <span className={[
              styles.dropdown,
              isSelected ? styles.selected : ''
            ].join(' ')}>
              <span>{item.title}</span>
              <i className="iconfont icon-arrow" />
            </span>
          </Flex.Item>
        })
      }
    </Flex>
  )

}