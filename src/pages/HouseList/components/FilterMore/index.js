import React, { Component } from 'react';

// 导入封装组件
import FilterFooter from '../../../../components/FilterFooter';

// 导入样式
import styles from './index.module.css';

export default class FilterMore extends Component {

  //构造方法
  constructor(props) {
    super(props);
  }

  //组件状态
  state = {
    //选中值,this.props.defaultValues从父组件传递给子组件的一个空数组
    selectedValues: this.props.defaultValues
  }

  // 渲染标签,该方法的参数不是父组件传递的对象data,而该参数是对象中键对应的数组,因此可以使用map来遍历
  renderFilters = (data) => {
    const { selectedValues } = this.state;
    // 高亮类名： styles.tagActive
    return data.map(item => {
      //判断当前项是否被选中,若被选中将添加类名让其高亮
      const isSelected = selectedValues.indexOf(item.value) > -1;
      return (
        <span
          key={item.value}
          className={[styles.tag, isSelected ? styles.tagActive : ''].join(' ')}
          //涉及到点击事件,一定要注意到this的指向问题
          onClick={() => this.onTagClick(item.value)}
        >{item.label}</span>
      )
    })
  }

  //点击目标,让选中值高亮
  onTagClick(value) {

    /**
     * 获取选中值并且高亮显示
     * 
     * 1.在state中添加状态 selectedValues
     * 2.给标签绑定单击事件，通过参数获取到当前项的value
     * 3.判断selectedValues中是否包含当前value值
     * 4.如果不包含，就将当前项的value添加到selectedValues数组中
     * 5.如果包含，就从selectedValues数组中移除（使用数组的splice方法，根据索引号删除）
     * 6.在渲染标签时，判断selectedValues数组中，是否包含当前项的value，包含，就添加高亮类
     */
    const { selectedValues } = this.state;
    // console.log("点击目标,让选中值高亮 ");

    //创建新数组,尽量不要操作原数组
    const newSelectedValues = [...selectedValues];

    if (selectedValues.indexOf(value) <= -1) {  //查找给定元素在数组中的第一个索引,若不存在将返回-1
      //不包含当前的value
      newSelectedValues.push(value)
    } else {  //说明已经存在
      //说明已经包含,需要移除
      const index = newSelectedValues.findIndex(item => item === value);  //findIndex()函数也是查找目标元素，找到就返回元素的位置，找不到就返回-1。
      newSelectedValues.splice(value, 1);  //移除这一项,原数组有这一项了,就把新数组移除,是为了新数组覆盖原数组时,没有多余项
    }

    // 设置选中状态,将选中的项纪录在新数组中,在覆盖追加到原数组中
    this.setState(() => {
      return {
        selectedValues: newSelectedValues
      }
    })
  }

  //为FilterMore组件清空按钮设置业务逻辑
  onClean() {
    // 清空选中的值
    this.setState(() => {
      return {
        selectedValues: []
      }
    })
  }

  //为FilterMore组件确定按钮设置业务逻辑
  onOk() {

    //获取从父组件(Filter组件)里面传递的onSave函数(即确定按钮的业务逻辑),以及type组件的显示和隐藏
    const { type, onSave } = this.props;
    //将对应的参数传递给父组件对应的函数
    onSave(type, this.state.selectedValues);
  }

  /**
   * 清除和确定按钮的逻辑处理
   * 
   * 1.设置FilterFooter组件的取消按钮文字为:清除
   * 2.点击取消按钮时，清空所有选中的项的值（selectedValues:[]）
   * 3.点击确定按钮时，讲当前选中项的值和type，传递给Filter父组件
   * 4.在Filter组件中的onSave方法中，接收传递过来的选中值，更新状态selectedValues
   */
  render() {
    // 通过this.props获取到从父组件传递的数据信息
    const { data: { roomType, oriented, floor, characteristic }, onCancel, type } = this.props;

    return (
      <div className={styles.root}>

        {/* 遮罩层 */}
        {/* 点击遮罩层关闭条件框(子组件隐藏) */}
        {/* 重点:点击事件要执行一个函数,方法体写函数调用   若直接写函数名相当于直接函数引用,若加上参数相当于直接调用,把调用结构给点击事件处,这显然是不正确的 */}
        <div className={styles.mask} onClick={() => onCancel(type)} />

        {/* 条件内容 */}
        <div className={styles.tags}>
          <dl className={styles.dl}>
            <dt className={styles.dt}>户型</dt>
            <dd className={styles.dd}>{this.renderFilters(roomType)}</dd>

            <dt className={styles.dt}>朝向</dt>
            <dd className={styles.dd}>{this.renderFilters(oriented)}</dd>

            <dt className={styles.dt}>楼层</dt>
            <dd className={styles.dd}>{this.renderFilters(floor)}</dd>

            <dt className={styles.dt}>房屋亮点</dt>
            <dd className={styles.dd}>{this.renderFilters(characteristic)}</dd>
          </dl>
        </div>

        {/* 底部按钮 */}
        <FilterFooter
          className={styles.footer}
          //设置FilterFooter组件的取消按钮文字为:清除,  将组件原本设置的属性覆盖掉
          cancelText="清除"
          //清除按钮的业务逻辑,不控制FilterMore组件的隐藏
          onCancel={() => this.onClean()}
          onOk={() => this.onOk()}
        />

      </div>
    )
  }
}
