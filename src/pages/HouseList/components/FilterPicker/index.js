import React, { Component } from 'react';

import { PickerView } from 'antd-mobile';

import FilterFooter from '../../../../components/FilterFooter';

/**
 * 获取选中值
 * 
 * 1.在FilterPicker组件中，添加状态value（用于获取PickerView组件的选中值）
 * 2.给PickerView组件添加配置项 onChange，通过参数获取到选中值，并更新状态 value
 * 3.在确定按钮的事件处理程序中，讲 type 和 value 作为参数传递给父组件
 */
export default class FilterPicker extends Component {

  //构造方法
  constructor(props) {
    super(props);
  }

  //组件状态
  state = {
    //只有在创建组件时,才会给该状态设置值,即该值就没有,那么在传递给父组件Filter组件,就没有数值,也就不能保留更改的默认选中的状态值
    value: this.props.defaultValue  //默认选中组件的状态,即父组件的值传递到子组件(该组件)中,设定为当前组件的状态
  }

  render() {

    const { onCancel, onSave, data, cols, type } = this.props;
    const { value } = this.state;  //解构赋值

    return (
      <>
        {/* 上面的标签是一个空标签,因为一个组件最外层只能有一个标签 */}

        {/* 选择器组件： */}
        {/* 给选择器组件传入数据源data,父组件传递给子组件 */}
        <PickerView
          data={data}
          //一旦监听了onChange事件,改变了value的值,那么这个组件成为了受控组件,所以要同步value的值
          value={value}
          cols={cols}
          // 参数val是ant组件库里面该组件监听选中的值,我们在将监听的值赋值给当前组件的状态
          onChange={(val) => {
            this.setState({
              value: val
            })
          }} />

        {/* 底部按钮 */}
        {/* 底部按钮,这个type由外界进行的传递,所以我们需要通过props来进行接收 */}
        <FilterFooter
          onCancel={() => onCancel(type)}
          onOk={() => onSave(type, value)}
        />
      </>
    )
  }
}
