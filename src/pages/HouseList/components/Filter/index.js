import React, { Component } from 'react';

// 导入封装好的子组件
import FilterTitle from '../FilterTitle/index.js';
import FilterPicker from '../FilterPicker/index.js';
import FilterMore from '../FilterMore/index.js';

// 导入自定义的axios,就是可以简写
import { instance } from '../../../../utils/api.js';

// 导入react-spring动画库
import { Spring } from 'react-spring/renderprops';

// 导入样式
import styles from './index.module.css';

// 标题高亮状态
const titleSelectedStatus = {
  area: false,
  mode: false,
  price: false,
  more: false
}

// 筛选默认选中的状态值
const selectedValues = {
  area: ["area", "null"],
  mode: ["null"],
  price: ["null"],
  more: []
}

/**
 * 定义openType，实现FilterPicker显示隐藏
 * 
 * 1.在Filter组件中，提供组件展示或隐藏的状态：openType
 * 2.在render方法中判断 openType的值为 area/mode/price 时，就显示 FilterPicker组件，以及遮罩层
 * 3.在 onTitleClick方法中，修改状态 openType为当前 type，展示对话框
 * 4.在Filter组件中，提供onCancel方法（作为取消按钮和遮罩层的事件）
 * 5.在onCancel方法中，修改状态 openType为空，隐藏对话框
 * 6.讲onCancel通过props传递给FilterPicker组件，在取消按钮的单击事件中调用该方法
 * 7.在Filter组件中，提供onSave方法，作为确定按钮的事件处理
 */

/**
 * 获取筛选条件数据
 * 
 * 1.在Filter组件中，发送请求，获取所有筛选条件数据
 * 2.将数据保存为状态：filtersData
 * 3.封装方法 renderFilterPicker 来渲染FilterPicker组件
 * 4.在方法中，根据openType的类型，从filtersData中获取需要的数据
 * 5.讲数据通过props传递给FilterPicker组件
 * 6.FilterPicker组件接收到数据后，讲其作为PickerView组件的data
 */
export default class Filter extends Component {

  //构造方法
  constructor(props) {
    super(props);
  }

  //组件状态
  state = {
    //标题高亮状态  ES6简化语法,即键值一样时,可以简写
    titleSelectedStatus,
    // 控制FilterPicker或 FilterMore组件的展示和隐藏
    openType: '',
    // 所有条件筛选数据,开始写错了,应该是一个对象
    filtersData: {},
    //筛选默认选中的状态值
    selectedValues
  }

  // 父组件定义点击标题修改标题高亮状态的函数,注意this的指向问题
  // 要实现完整的功能,要结合后面的组件配合一起使用
  onTitleClick = type => {

    /**
     * 点击标题时,遍历标题高亮数据
     * 
     * 1.在标题点击事件 onTitleClick事件里面，获取到两个状态：标题选中状态对象和筛选条件的选中值对象
     * 2.根据当前标题选中状态对象，获取到一个新的标题选中状态对象（newTitleSelectedStatus）
     * 3.使用Object.keys()，遍历标题选中状态对象
     * 4.先判断是否为当前标题，如果是，直接让该标题选中状态为true（高亮）
     * 5.否则，分别判断每个标题的选中值是否与默认值相同  如果不同，则设置该标题的选中状态为true  如果相同，则设置该标题的选中状态为false
     * 6.更新状态 titleSelectedStatus的值为： newTitleSelectedStatus
     */

    //  给body增加样式,当展示Filter组件以及遮罩层时,就要让房屋列表不能滚动了
    // 在该组件被加载的生命周期钩子函数中,获取body元素的dom对象,然后再给body元素设置样式
    this.htmlBody.className = 'body-fixed';

    const { titleSelectedStatus, selectedValues } = this.state;

    //创建新的标题选中状态对象
    let newTitleSelectedStatus = { ...titleSelectedStatus };

    //标题选中titleSelectedStatus是一个对象,不容易遍历,采用ES6中Object.keys()方法,把要遍历的对象作为参数传递给该方法,会得到一个数组在遍历
    //数组返回值就是对象的键的集合 Object.keys(titleSelectedStatus) => ["area","mode","price","more" ]
    Object.keys(titleSelectedStatus).forEach(key => {

      //key表示数组中的每一项
      if (key === type) {
        //当前标题
        newTitleSelectedStatus[type] = true;
        return;
      }

      // 其他标题
      let selectedVal = selectedValues[key];

      //其他标题,在不是默认设置的情况下,让标题高亮
      if (key === "area" && (selectedVal.length !== 2 || selectedVal[0] !== "area")) {
        //修改默认选中,让其高亮
        newTitleSelectedStatus[key] = true;
      } else if (key === "mode" && selectedVal[0] !== "null") {
        //修改默认选中,让其高亮
        newTitleSelectedStatus[key] = true;
      } else if (key === "price" && selectedVal[0] !== "null") {
        //修改默认选中,让其高亮
        newTitleSelectedStatus[key] = true;
      } else if (key === "more" && selectedVal.length !== 0) {
        //修改默认选中,让其高亮
        newTitleSelectedStatus[key] = true;
      } else {
        newTitleSelectedStatus[key] = false;
      }

    })

    console.log("newTitleSelectedStatus", newTitleSelectedStatus);

    this.setState({
      //使用新的标题选中状态对象来更新
      titleSelectedStatus: newTitleSelectedStatus,
      // 修改状态 openType为当前 type，展示对话框
      openType: type
    })

    /** 
    this.setState({
      // 当点击哪一个标题时,该标题的状态就设置为true,默认titleSelectedStatus状态全为false
      // [type]: true 注意这是ES6的属性名表达式,又使用到ES6的扩展运算符,后面覆盖掉前面的属性
      //在此处遇到坑了,修改标题的状态,应当使用状态里面的值,这样状态的改变会累积的,而直接使用titleSelectedStatus定义变量的值,相当于每一次修改都是重新修改,之前的修改不累积
      //再次理解了:课件里面是直接写组件选中状态titleSelectedStatus,但是课件里面已经在类组件的状态里面结构赋值了

      // titleSelectedStatus: {
      //   ...titleSelectedStatus,
      //   [type]: true
      // },
      // 修改状态 openType为当前 type，展示对话框
      openType: type
    })
    */
  }

  // 提供onCancel方法（作为取消按钮和点击遮罩层的事件）
  onCancel = type => {

    // 在取消条件筛选后,要把body元素的样式取消掉,这样页面就又可以再次滚动了
    this.htmlBody.className = '';

    //在关闭对话框时（onCancel），根据type和当前type的选中值，判断当前菜单是否高亮
    const { titleSelectedStatus, selectedValues } = this.state;
    //创建新的标题选中状态对象,
    let newTitleSelectedStatus = { ...titleSelectedStatus };
    //获取选中的值,和onSave方法不一样,参数没有传递value的值
    let selectedVal = selectedValues[type];

    if (type === "area" && (selectedVal.length !== 2 || selectedVal[0] !== "area")) {
      newTitleSelectedStatus[type] = true;
    } else if (type === "mode" && selectedVal[0] !== "null") {
      newTitleSelectedStatus[type] = true;
    } else if (type === "price" && selectedVal[0] !== "null") {
      newTitleSelectedStatus[type] = true;
    } else if (type === "more" && selectedVal.length !== 0) {
      newTitleSelectedStatus[type] = true;
    } else {
      newTitleSelectedStatus[type] = false;
    }

    this.setState({
      // 修改状态 隐藏对话框
      openType: '',
      //若取消按钮,没有选择(修改默认状态)就不让标题栏高亮
      titleSelectedStatus: newTitleSelectedStatus
    })
  }

  // 提供onSave方法（作为确认按钮的事件）
  onSave = (type, value) => {
    console.log("父组件获取子组件的选中值", type, value);

    // 在确认条件筛选后,要把body元素的样式取消掉,这样页面就又可以再次滚动了
    this.htmlBody.className = '';

    const { titleSelectedStatus } = this.state;
    //创建新的标题选中状态对象,
    let newTitleSelectedStatus = { ...titleSelectedStatus };
    let selectedVal = value;  //选中的值

    //点击确定按钮时,只有选择条件(即表示默认状态)才让对应的标题栏高亮
    if (type === "area" && (selectedVal.length !== 2 || selectedVal[0] !== "area")) {
      newTitleSelectedStatus[type] = true;
    } else if (type === "mode" && selectedVal[0] !== "null") {
      newTitleSelectedStatus[type] = true;
    } else if (type === "price" && selectedVal[0] !== "null") {
      newTitleSelectedStatus[type] = true;
    } else if (type === "more" && selectedVal.length !== 0) {
      newTitleSelectedStatus[type] = true;
    } else {
      newTitleSelectedStatus[type] = false;
    }

    /**
     * 组装筛选条件
     * 
     * 1.在Filter组件的onSave方法中，根据最新selectedValues组装筛选的条件数据 filters
     * 2.获取区域数据的参数名：area 或 subway(选中值，数组的第一个元素)
     * 3.获取区域数据值（以最后一个value为准）
     * 4.获取方式和租金的值（选中值得第一个元素）
     * 5.获取筛选（more）的值（讲选中值数组转换为以逗号分隔的字符串）
     * {
        area: 'AREA|67fad918-f2f8-59df', // 或 subway: '...'
        mode: 'true', // 或 'null'
        price: 'PRICE|2000',
        more: 'ORIEN|80795f1a-e32f-feb9,ROOM|d4a692e4-a177-37fd'
      }
     * 
     */

    //定义一个变量存储状态变量,可高复用(即用于条件查询数据的封装)
    let newSelectedValues = {
      ...this.state.selectedValues,
      // 只更新当前的type对应的选中值
      [type]: value
    }

    //把条件筛选的状态数据,进行结构赋值
    const { area, mode, price, more } = newSelectedValues;

    //定义存储筛选条件数据的变量
    const filters = {}

    //区域数据筛选
    const areaKey = area[0];  //根据用户选择是 "area" 还是 "subway" 那么键就是area数组的第一项
    let areaValue = "null";  //定义条件筛选对象中区域键对应的值,默认为null
    //想通了,数组长度为2(即选择区域或地铁时,后面是不限,那相当于没有选择条件,看了看)
    if (area.length === 3) {
      areaValue = area[2] != "null" ? area[2] : area[1];
    }
    //筛选条件数据对象filters存储键对应的值
    filters[areaKey] = areaValue;

    //方式,租金数据筛选
    filters.mode = mode[0];  //方式数组只有一项,将选择的一项存储到筛选条件数据对象filters存储键mode对应的值
    filters.price = price[0];  //租金数组只有一项,将选择的一项存储到筛选条件数据对象filters存储键price对应的值

    //标题栏中"筛选"栏的数据筛选
    filters.more = more.join(",");  //标题栏中"筛选"栏的数据是一个数组,数组长度由用户选择多少决定,将数组转化为字符串

    //子组件向父组件通信,将本组件中封装的条件筛选数据传递给父组件中,用于根据条件筛选获取房屋信息列表
    this.props.onFilter(filters);

    //点击确定按钮有默认状态被修改时才让标题栏高亮
    this.setState(() => {
      return {
        // 修改状态 隐藏对话框
        openType: '',
        //修改了默认状态才让标题栏高亮
        titleSelectedStatus: newTitleSelectedStatus,

        //点击标题栏选择状态,修改的状态更新到对应的标题栏下
        //将选择状态的数据封装到一个变量里面,可高复用(即用于条件查询数据的封装)
        selectedValues: newSelectedValues
      }
    })

    /** 
    this.setState({
      // 修改状态 隐藏对话框
      openType: '',
      //解构赋值,获取到父组件(该组件)的选中状态,只更新当前的type对应的选中值
      //在此处遇到坑了,修改标题的状态,应当使用状态里面的值,这样状态的改变会累积的,而直接使用titleSelectedStatus定义变量的值,相当于每一次修改都是重新修改,之前的修改不累积
      selectedValues: {
        ...this.state.selectedValues,
        // 只更新当前的type对应的选中值
        [type]: value
      }
    })
    */

  }

  // 渲染FilterPicker组件的方法
  renderFilterPicker() {
    // 结构赋值filtersData(通过后台请求获取赋值给状态filtersData)获取对应数据 roomType，oriented，floor，characteristic
    const { openType, filtersData: { area, subway, rentType, price }, selectedValues } = this.state;

    if (openType != 'area' && openType != 'mode' && openType != 'price') {
      return null;
    }

    // 根据openType来拿到当前筛选条件数据
    let data = [];
    // 将数据传递给FilterPicker组件,让ant组件库里面的选择器组件要展示几列数据,默认是模型数据,不起作用
    let cols = 3;
    // 默认选中值,得到每一个数组
    let defaultValue = selectedValues[openType];

    switch (openType) {
      case 'area':
        // 获取到区域选项数据
        data = [area, subway];
        cols = 3;
        break;

      case 'mode':
        // 获取到方式选项数据
        data = rentType;
        cols = 1;
        break;

      case 'price':
        // 获取到租金选项数据
        data = price;
        cols = 1;
        break;

      default:
        break;
    }

    return <FilterPicker
      //给FilterPicker组件添加key值,在不同标题之间切换时候，key值都不相同，React内部会在key不同时候，重新创建该组件
      key={openType}
      onCancel={this.onCancel}
      onSave={this.onSave}
      data={data}
      cols={cols}
      type={openType}
      defaultValue={defaultValue}
    />
  }

  /**
   * 渲染FilterMore组件的方法
   * 
   * 1.封装renderFilterMore方法，渲染FilterMore组件
   * 2.从filtersData中，获取数据（roomType，oriented，floor，characteristic），通过props传递给FilterMore组件
   * 3.FilterMore组件中，通过props获取到数据，分别将数据传递给renderFilters方法
   * 4.正在renderFilters方法中，通过参数接收数据，遍历数据，渲染标签
   */
  renderFilterMore() {
    // 结构赋值filtersData(通过后台请求获取赋值给状态filtersData)获取对应数据 roomType，oriented，floor，characteristic
    const { openType, filtersData: { roomType, oriented, floor, characteristic }, selectedValues } = this.state;

    if (openType != 'more') {
      return null;
    }

    //把数据封装为一个对象,方便传递
    const data = {
      // ES6简化语法,即键值一样时,可以简写
      roomType,
      oriented,
      floor,
      characteristic
    }

    // 默认选中值,openType得到标题栏筛选项,将定义的默认空数组赋值给变量defaultValue
    // let defaultValue = selectedValues[openType];  //其中openType的值就是more
    const defaultValue = selectedValues[openType];

    return <FilterMore
      //传递给子组件的数据,为一个对象
      data={data}
      //为FilterMore组件中确定按钮的业务逻辑传递给子组件里面
      onSave={this.onSave}
      //组件的显示隐藏状态
      type={openType}
      //默认选中值
      defaultValues={defaultValue}
      //取消事件,隐藏组件状态,传递给子组件给遮罩层使用
      onCancel={this.onCancel}
    />
  }

  // 获取筛选数据
  async getFilterData() {
    let { value } = JSON.parse(localStorage.getItem('local_City'));
    let { data: res } = await instance.get(`/houses/condition?id=${value}`);
    console.log(" 获取筛选数据", res);

    this.setState(() => {
      return {
        // 将请求到的筛选数据赋值给状态模型数据
        filtersData: res.body
      }
    })
    // console.log(this.state.filtersData);
  }

  /**
   * 实现遮罩层动画
   * 
   * 1.创建方法 renderMask来渲染遮罩层 div
   * 2.修改渲染遮罩层的逻辑，保证Spring组件一直都被渲染（Spring组件被销毁了，就无法实现动画效果）
   * 3.修改to属性的值，在遮罩层隐藏时为0，在遮罩层展示为1
   * 4.在render-props的函数内部，判断props.opacity是否等于0
   * 5.如果等于0，就返回null，解决遮罩层遮挡页面导致顶部点击事件失效
   * 6.如果不等于0，渲染遮罩层div
   */
  // 渲染遮罩层div
  renderMask() {

    // 从状态中解构出openType展示和隐藏组件
    const { openType } = this.state;

    const isHide = openType === 'more' || openType === '';

    // Spring组件被销毁了，就无法实现动画效果   这遮罩层组件中,Spring组件一直存在

    return (
      // 使用react-spring动画库组件包裹div元素
      /**
       * 这里涉及到React Render Props 模式的学习,之后时间还要巩固一下
       * 
       * Render Props模式是一种非常灵活复用性非常高的模式，它可以把特定行为或功能封装成一个组件，提供给其他组件使用让其他组件拥有这样的能力
       */
      <Spring
        // 给Spring组件添加from属性，指定：组件第一次渲染时的动画状态
        from={{ opacity: 0 }}
        // 给Spring组件添加to属性，指定：组件要更新的新动画状态
        to={{ opacity: isHide ? 0 : 1 }}
      >
        {props => {

          //说明遮罩层已近完成动画效果,隐藏了
          if (props.opacity === 0) {
            return null;
          }

          return (
            <div
              // props就是透明度有0~1中变化的值
              style={props}
              className={styles.mask}
              // 点击遮罩层,触发事件关闭遮罩层
              onClick={() => this.onCancel(openType)}>
            </div>
          )
        }}
      </Spring>
    )
  }

  // 生命周期钩子函数
  componentDidMount() {

    //获取到body元素的dom对象
    this.htmlBody = document.body;

    //调用获取筛选数据的方法
    this.getFilterData();
  }

  /**
   * react-spring动画库的基本使用
   * 
   * 1.安装： yarn add react-spring
   * 2.打开Spring组件文档
   * 3.导入Spring文档，使用Spring组件包裹要实现动画效果的遮罩层div
   * 4.通过render-props模式，讲参数props设置为遮罩层div的style
   * 5.给Spring组件添加from属性，指定：组件第一次渲染时的动画状态
   * 6.给Spring组件添加to属性，指定：组件要更新的新动画状态
   * 7.props就是透明度有0~1中变化的值
   */
  render() {

    /**
     * titleSelectedStatus:标题高亮状态
     * openType:控制FilterPicker或 FilterMore组件的展示和隐藏
     */
    const { titleSelectedStatus, openType } = this.state;

    return (
      <div className={styles.root}>

        {/* 前三个菜单的遮罩层,单独抽取封装方法,来渲染遮罩层组件 */}
        {this.renderMask()}

        <div className={styles.content}>
          {/* 标题栏 */}
          {/* 父组件传递回调函数,子组件调用,在父组件内更改状态,再将最新的状态传递到子组件里面 */}
          <FilterTitle
            //标题高亮状态,传递给子组件,用于
            titleSelectedStatus={titleSelectedStatus}
            //将FilterFooter组件里面的按钮,修改筛选默认选中的状态值,将最新的值传递给FilterTitle组件
            // selectedValues={selectedValues}
            //父组件定义函数改变标题高亮状态titleSelectedStatu,在将最新的组件状态值传递给子组件
            onClick={this.onTitleClick} />

          {/* 前三个菜单栏对应的内容 */}
          {this.renderFilterPicker()}

          {/* 最后一个菜单对应的内容： */}
          {this.renderFilterMore()}
        </div>
      </div>
    )
  }

}
