import React, { Component } from 'react';

// 导入ant组件库组件
import { Flex, WingBlank, WhiteSpace, Toast } from 'antd-mobile';

// 导入React官方跳转路由标签
import { Link } from 'react-router-dom';

// 现在创建了axios的配置文件,因此上面的导入axios就不需要了,需导入新的axios配置
import { instance } from '../../utils/api.js';

// 导入顶部导航栏组件
import NavHeader from '../../components/NavHeader';

// 导入组件样式
import styles from './index.module.css';

// 验证规则：
// const REG_UNAME = /^[a-zA-Z_\d]{5,8}$/
// const REG_PWD = /^[a-zA-Z_\d]{3,12}$/

class Registe extends Component {

  // 组件状态
  state = {
    // 账号用户名
    username: '',
    // 账号密码
    password: '',
    // 再次输入重复密码
    repeatPassword: ''
  }

  // 监听表单输入的数据赋值设置为组件的状态
  getUsername = e => {
    this.setState({
      username: e.target.value
    })
  }

  // 监听表单输入的数据赋值设置为组件的状态
  getPassword = e => {
    this.setState({
      password: e.target.value
    })
  }

  // 监听表单输入的数据赋值设置为组件的状态
  getRepeatPassword = e => {
    this.setState({
      repeatPassword: e.target.value
    })
  }

  // 表单事件提交的处理函数
  handleSubmit = async (e) => {
    // 阻止表单提交的默认行为,就是提交表单页面刷新
    e.preventDefault()

    // 获取账号,密码,重复密码
    const { username, password, repeatPassword } = this.state;

    // 判断输入的密码和重复密码是否一致
    if (password == repeatPassword) {

      const { data: res } = await instance.post('/user/registered', { username, password })
      console.log("注册账号打印输出信息", res);

      // 从后台获取的数据解构出需要的值
      const { status, body, description } = res;
      if (status === 200) {
        // 注册成功
        // localStorage.setItem("login_Token", body.token);
        // 编程式导航
        this.props.history.go(-1);
      } else {
        // 如输入的账号名重复等提示
        Toast.info(description, 2, null, false)
      }
    } else {
      // 注册密码两次不一致
      Toast.info("两次密码输入的不一致", 2, null, false)
    }

  }

  render() {

    const { username, password, repeatPassword } = this.state;

    return (
      <div className={styles.root}>
        {/* 顶部导航 */}
        <NavHeader className={styles.navHeader}>注册</NavHeader>
        <WhiteSpace size="xl" />
        <WingBlank>
          <form onSubmit={this.handleSubmit}>
            <div className={styles.formItem}>
              <label className={styles.label}>用户名</label>
              <input
                className={styles.input}
                placeholder="请输入账号"
                name="username"
                // 将组件状态的值设置为表单的值
                value={username}
                // 监听表单输入的值设置为组件的状态
                onChange={this.getUsername}
              />
            </div>
            <div className={styles.formItem}>
              <label className={styles.label}>密码</label>
              <input
                className={styles.input}
                type="password"
                placeholder="请输入密码"
                name="password"
                // 将组件状态的值设置为表单的值
                value={password}
                // 监听表单输入的值设置为组件的状态
                onChange={this.getPassword}
              />
            </div>
            <div className={styles.formItem}>
              <label className={styles.label}>重复密码</label>
              <input
                className={styles.input}
                type="password"
                placeholder="请重新输入密码"
                name="repeatPassword"
                // 将组件状态的值设置为表单的值
                value={repeatPassword}
                // 监听表单输入的值设置为组件的状态
                onChange={this.getRepeatPassword}
              />
            </div>
            <div className={styles.formSubmit}>
              <button className={styles.submit} type="submit">
                注册
              </button>
            </div>
          </form>

          <Flex className={styles.backHome} justify="between">
            <Link to="/home">点我回首页</Link>
            <Link to="/login">已有账号，去登录</Link>
          </Flex>
        </WingBlank>
      </div>
    )
  }
}

export default Registe;
