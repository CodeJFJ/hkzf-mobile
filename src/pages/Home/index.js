import React from 'react';

// 导入路由
import { Route } from 'react-router-dom';

// 导入组件自己的样式文件
import './index.css'

// 导入子组件
import Index from '../Index';
import HouseList from '../HouseList';
import News from '../News';
import Profile from '../Profile';

// 导入TabBar组件
import { TabBar } from 'antd-mobile';

// 声明数据源
const tabItems = [
  { title: '首页', icon: 'icon-ind', path: '/home' },
  { title: '找房', icon: 'icon-findHouse', path: '/home/list' },
  { title: '咨询', icon: 'icon-infom', path: '/home/news' },
  { title: '我的', icon: 'icon-my', path: '/home/profile' }
]

export default class CityList extends React.Component {
  // 定义TabBar组件状态
  state = {
    // 默认选中的TabBar菜单项,记录当前选中的pathname值来匹配对应的TabBar
    selectedTab: this.props.location.pathname,
    // 是否展示TabBar菜单项展示与隐藏(默认)
    // hidden: false,
    // 控制是否全屏
    // fullScreen: true,
  };

  // 定义函数,封装代码块,简化代码
  renderTabBarItem() {
    return tabItems.map(item => {
      return (
        <TabBar.Item
          title={item.title}
          key={item.title}
          icon={<i className={`iconfont ${item.icon}`}></i>
          }
          selectedIcon={<i className={`iconfont ${item.icon}`}></i>
          }
          // 在TabBar中每一项,selected来判断当前项是否选中
          selected={this.state.selectedTab === item.path}
          // bar 点击触发，需要自己改变组件 state & selecte={true}
          onPress={() => {
            this.setState({
              selectedTab: item.path,
            });
            // 编程式导航切换路由
            this.props.history.push(item.path)
          }}
        >
        </TabBar.Item>
      )
    })
  }

  // 生命周期钩子函数
  componentDidUpdate(prevProps) {
    // 切换导航菜单栏让对应的TabBar高亮
    // if语句判断点前路由与上一次路由是否一样,即判断路由是否切换
    if (this.props.location.pathname != prevProps.location.pathname) {
      // 该钩子函数可以更改状态,但一定要写逻辑判断里面,否则会出现递归更新出现组件错误
      this.setState({
        selectedTab: this.props.location.pathname,
      });
    }
  }

  render() {
    return <div className='home'>
      {/* 定义路由 */}

      {/* 首页实现 */}
      {/*添加 exact属性,由模糊匹配改为精确匹配  */}
      <Route exact path='/home' component={Index}></Route>
      {/* 匹配默认路由,在App.js中定义,与home路由平齐 */}

      <Route path='/home/list' component={HouseList}></Route>
      <Route path='/home/news' component={News}></Route>
      <Route path='/home/profile' component={Profile}></Route>


      {/* 导入TabBar组件 */}
      <TabBar
        unselectedTintColor="#888"  //默认是#888
        tintColor="#21b97a"
        barTintColor="white"
        noRenderContent="true"  //不渲染TabBar内容部分
      >
        {/* 把TabBar每一项封装为一个函数,遍历每一项,调用函数 */}
        {this.renderTabBarItem()}
      </TabBar>

    </div>
  }
}