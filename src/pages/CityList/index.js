import React from 'react';

// 引入axios发送请求库
// import axios from 'axios';
// 现在创建了axios的配置文件,因此上面的导入axios就不需要了,需导入新的axios配置
import { instance } from '../../utils/api.js';

// 导入antd-mobile库中组件
import { Toast } from 'antd-mobile';

// 导入react-virtualized库中组件
import { List, AutoSizer } from 'react-virtualized';

// 导入城市列表的样式
import './index.scss'

// 导入当前城市信息
import { getCurrentCity } from '../../utils/index.js'

// 导入封装NavBar组件
import NavHeader from '../../components/NavHeader/index.js'

// 定义一个函数,对城市信息进行格式化
const formatCityData = list => {
  // 定义一个空对象,作为容器存放排序对应的城市
  let cityList = {};
  // 遍历为排序的每一个城市信息对象
  list.forEach(item => {
    // 遍历得到每个城市信息对象,获得城市名切割得到城市首字母,用于上面定义空对象的键
    let first = item.short.substr(0, 1);
    // 对城市列表首字母进行归类,若已经存在该首字母,将每一个城市信息对象存放在cityList对象的键对应的值(即为数组)中
    if (cityList[first]) {
      // cityList[frist]获取cityList对象的键对应的值(即为数组)
      cityList[first].push(item)
    } else {
      // 对城市列表首字母进行归类,若不存在该首字母,item为城市信息对象每一项(即为对象),[item]就为数组
      cityList[first] = [item]
    }
  })
  // Object.keys()方法返回得到容器cityList对象中所有的键所组成的数组,有点类似Java里面操作map的keySet()方法,已得到数组再对数组进行sort排序
  let cityIndex = Object.keys(cityList).sort();
  // 定义该函数进行格式化,返回处理后的城市列表cityList和城市索引cityIndex
  return {
    cityList,
    cityIndex
  }
}

// 定义函数,封装处理字母索引的方法
const formatCityIndex = (letter) => {
  // 通过switch语句格式化一些数据
  switch (letter) {
    case '#':
      return '当前定位'
    case 'hotCity':
      return '热门城市'
    default:
      return letter.toUpperCase()
  }
}

// 定义函数,封装处理字母索引的方法
const formatCityName = (letter) => {
  // 通过switch语句格式化一些数据
  switch (letter) {
    case 'hotCity':
      return '热'
    default:
      return letter.toUpperCase()
  }
}

// 定义常量
const TITLE_HEIGHT = 36;  //城市列表索引名称高度
const NAME_HEIGHT = 50;  //城市列表城市名称高度
const HOUSE_CITY = ["北京", "上海", "广州", "深圳"]  //定义数组,存在房源信息的城市

export default class CityList extends React.Component {

  constructor(props) {
    super(props)

    // 组件状态
    this.state = {
      cityList: {},  //城市列表cityList
      cityIndex: [],  //城市索引cityIndex
      activeIndex: 0 //指定右侧索引列表索引号高亮的索引号
    }

    // 创建react引用,该方法返回一个ref 对象
    this.citylistComponent = React.createRef()
  }

  // 获取请求城市列表数据
  async getCityList() {
    // 获取所有城市列表数据
    let { data: res } = await instance.get('/area/city?level=1');
    // console.log(res); //打印得到请求所有城市列表数据
    // 调用函数进行城市信息格式化(类似Java的工具类),返回得到处理后的城市列表 cityList和城市索引cityIndex
    let { cityList, cityIndex } = formatCityData(res.body);
    // console.log(cityList, cityIndex);  处理后的城市列表cityList和城市索引cityIndex
    // 获取热门城市列表数据
    let { data: hotRes } = await instance.get('/area/hot')
    // console.log(hotRes); //打印得到请求热门城市列表数据
    // 将热门城市添加到城市列表中
    cityList['hotCity'] = hotRes.body
    // 将热门城市放在城市索引前面
    cityIndex.unshift('hotCity')
    // console.log(cityList, cityIndex);  //处理后的城市列表cityList和城市索引cityIndex
    let CurrentCity = await getCurrentCity()  //备注:在此遇到坑了,调用函数得到的是对象,而在城市列表cityList对象中键对应的值是数组,数组里面放的是对象,尽管只有一个对象也要放在数组中,保持一致,方便遍历渲染
    // 将当前城市添加到城市列表中
    cityList['#'] = [CurrentCity]  //就在这个地方遇到坑了,当前城市信息保存在对象中,尽管只有一项,也要放在数组中
    // 将当前城市放在城市索引前面
    cityIndex.unshift('#')
    // console.log(cityList, cityIndex); //处理后的城市列表cityList和城市索引cityIndex
    // 将城市列表cityList和城市索引cityIndex保存到组件的状态中
    this.setState(() => {
      return {
        cityList,
        cityIndex
      }
    })
  }

  // List组件渲染每一行的内容的函数
  // 将react-virtualized库中组件AutoSizer每一行渲染内容函数作为react组件内的一个函数
  rowRenderer = ({
    key,         // Unique key within array of rows
    index,       // 索引号
    isScrolling, // 当前项是否正在滚动中
    isVisible,   // 当前项在List中是可见的
    style        // 重点属性：一定要给每一个行数添加该样式
  }) => {
    // 注意该函数要使用箭头函数,改变this指向,这样才能通过this.state解构赋值获取到城市列表cityList和城市索引cityIndex
    const { cityIndex, cityList } = this.state;
    const letter = cityIndex[index];

    return (
      <div key={key} style={style} className='city'>
        <div className='title'>{formatCityIndex(letter)}</div>
        {cityList[letter].map(item => <div className='name' key={item.value} onClick={() => { this.changeCity(item) }}>{item.label}</div>)}
      </div>
    )
  }

  // 动态计算城市列表每一行高度的方法
  getRowHeight = ({ index }) => {
    //通过解构赋值获取到每一个List列表索引
    // 注意该函数要使用箭头函数,改变this指向,这样才能通过this.state解构赋值获取到城市列表cityList和城市索引cityIndex
    const { cityIndex, cityList } = this.state;
    // 索引标题高度 + 城市数量 * 城市标题高度
    // TITLE_HEIGHT + cityList[cityIndex[index]].length * NAME_HEIGHT
    // console.log(index);  打印输出,通过解构赋值获取到每一个List列表索引
    return TITLE_HEIGHT + cityList[cityIndex[index]].length * NAME_HEIGHT
  }

  // 获取当前列表渲染行的信息
  onRowsRendered = ({ startIndex }) => {
    //  // 注意该函数要使用箭头函数,改变this指向
    // console.log(startIndex);
    if (this.state.activeIndex != startIndex) {
      this.setState(() => {
        return {
          activeIndex: startIndex
        }
      })
    }
  }

  // 封装渲染右侧索引列表的方法
  renderCityIndex() {
    const { cityIndex, activeIndex } = this.state;
    // 获取到cityIndex,遍历并渲染
    // 三元表达式指定当前项索引号高亮
    return cityIndex.map((item, index) => (
      <li
        className='city-index-item'
        key={index}
        onClick={() => {
          // 拿到List组件的实例,通过List组件的Ref引用
          // this.listComponent.current获取到组件实例,该对象下的current属性就指向了绑定的元素或组件对象
          this.citylistComponent.current.scrollToRow(index)
        }}>
        <span className={activeIndex == index ? 'index-active' : ''}>{formatCityName(item)}</span>
      </li>))
  }

  // 切换城市,获取切换城市的信息,保存到本地缓存LocalStorage中
  // 调用函数参数传递的是一个对象,通过解构赋值只需要使用其中的一些属性
  changeCity({ label, value }) {
    // 判断条件为切换城市的选项是否在有房源信息的城市里面
    if (HOUSE_CITY.indexOf(label) > -1) {

      // 如果有将切换的城市的房源信息存储在本地缓存中
      localStorage.setItem('local_City', JSON.stringify({ label, value }))
      this.props.history.go(-1);
    } else {
      // Toast为ant组件库中的组件,提示信息组件,第二个参数是多久之后提示框消失,第四个参数是提示框信息存在是否有遮罩层,即显示信息时不能滑动页面
      Toast.info('该城市暂无房源信息', 1, null, false)
    }
  }

  // 生命周期钩子函数
  async componentDidMount() {
    // 调用获取请求城市列表数据函数
    await this.getCityList();

    // 提前计算List中每一行的高度,实现scrollToRow的精确跳转,调用List组件的measureAllRows方法
    // 调用这个方法的时候需要保证List组件中已经有数据了,如果List组件中数据为空,就会导致错误
    // 解决:只有等异步方法this.getCityList()调用结束后,再执行下面的方法即可
    this.citylistComponent.current.measureAllRows();
  }

  render() {
    return <div className='citylist'>

      {/* 顶部导航栏组件 */}
      {/* 挂载封装组件 */}
      <NavHeader>城市选择</NavHeader>

      {/* 城市列表 */}
      {/* 这里涉及到react高阶组件render-props 模式 */}
      <AutoSizer>
        {/* 箭头函数参数采用解构赋值 */}
        {({ width, height }) => (
          <List
            // 创建List组件的引用,通过ref 属性绑定该对象  
            ref={this.citylistComponent}
            // 组件的宽度
            width={width}
            // 组件的高度
            height={height}
            // 行数(例如A,B,C)
            rowCount={this.state.cityIndex.length}
            // 每行的高度
            rowHeight={this.getRowHeight}
            // 渲染每一行的内容
            rowRenderer={this.rowRenderer}
            // 获取当前列表渲染行的信息
            onRowsRendered={this.onRowsRendered}
            // 滚动对齐与开始对齐,(补充:该属性存在一些问题,点击索引的城市没有渲染,存在点击索引切换城市有精度问题)
            scrollToAlignment='start'
          />
        )}
      </AutoSizer>

      {/* 城市列表右侧索引 */}
      <ul className='city-index'>
        {this.renderCityIndex()}
      </ul>
    </div>
  }
}