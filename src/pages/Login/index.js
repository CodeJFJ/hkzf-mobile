import React, { Component } from 'react';

// 导入ant组件库组件   WingBlank两翼留白,  WhiteSpace上下留白  Toast提示
import { Flex, WingBlank, WhiteSpace, Toast } from 'antd-mobile';

// 导入React官方跳转路由标签
import { Link } from 'react-router-dom';

// 现在创建了axios的配置文件,因此上面的导入axios就不需要了,需导入新的axios配置
import { instance } from '../../utils/api.js';

// 导入顶部导航栏组件
import NavHeader from '../../components/NavHeader/index.js';

// 导入withFormik高阶组件
import { withFormik, Form, Field, ErrorMessage } from 'formik';

// 导入Yup
import * as Yup from 'yup';

// 导入组件样式
import styles from './index.module.css';

// 验证规则：
const REG_UNAME = /^[a-zA-Z_\d]{5,8}$/;
const REG_PWD = /^[a-zA-Z_\d]{5,12}$/;

/**
 * 用户登录功能实现
 * 
 * 1.添加状态：username和password
 * 2.使用受控组件方式获取表单元素值
 * 3.给form表单添加 onSubmit
 * 4.创建方法 handleSubmit，实现表单提交
 * 5.在方法中，通过username和password获取到账号和密码
 * 6.使用API调用登录接口，将username和password作为参数
 * 7.判断返回值status为200时候，表示登录成功
 * 8.登录成功后，将token保存到本地存储中（hkzf_token）
 * 9.返回登录前的页面
 */
class Login extends Component {

  // 组件状态
  state = {
    // 账号用户名
    username: '',
    // 账号密码
    password: ''
  }

  /*
  注释到80多行,不在使用自己在组件里面定义的函数了,而使用高阶组件里面的函数了

  // 监听表单输入的数据赋值设置为组件的状态  开始this.setState()方法里面写回调函数不行,接收不到键盘输入的值,因此又改为了对象形式
  getUsername = e => {
    this.setState({
      username: e.target.value
    })
  }

  // 监听表单输入的数据赋值设置为组件的状态
  getPassword = e => {
    this.setState({
      password: e.target.value
    })
  }

  // 表单事件提交的处理函数
  handleSubmit = async (e) => {
    // 阻止表单提交的默认行为,就是提交表单页面刷新
    e.preventDefault()

    // 获取账号和密码
    const { username, password } = this.state;

    const { data: res } = await instance.post('/user/login', { username, password })
    console.log("提交登录表单事件");

    // 从后台获取的数据解构出需要的值
    const { status, body, description } = res;

    if (status === 200) {
      // 登录成功,将数据存入到token中
      localStorage.setItem("login_Token", body.token);
      // 编程式导航
      this.props.history.go(-1);
    } else {
      // 登录失败
      Toast.info(description, 2, null, false)
    }

  }

  */

  render() {

    const { username, password } = this.state;

    // 通过props获取到values（表单元素值对象），handleSubmit表单提交时间，handleChange监听表单改变,handleBlur监听表单失去焦点,touched 被访问过 必须要有失去焦点
    // const { values, handleSubmit, handleChange, handleBlur, errors, touched } = this.props;
    // 去掉所有 props,导入 Form组件,Field组件,ErrorMessage 组件,这些从高阶组件里面导入的属性,就没有作用了,可以注释掉

    // console.log( values, handleSubmit, handleChange );
    // console.log(errors, touched);

    return (
      <div className={styles.root}>

        {/* 顶部导航 */}
        <NavHeader className={styles.navHeader}>账号登录</NavHeader>

        {/* 设置上下留白 */}
        <WhiteSpace size="xl" />

        {/* 登录表单 */}
        <WingBlank>

          {/* 使用handleSubmit设置为表单的onSubmit,就不在使用组件自己的函数了,将表单的事件指向高阶组件中定义的函数 */}
          {/* <form onSubmit={handleSubmit}> */}

          {/* 导入 Form组件，替换form元素，去掉onSubmit */}
          <Form>

            <div className={styles.formItem}>
              {/* <input
                className={styles.input}
                // 用户名和密码监听使用的是一个函数,是通过表单的name属性来判断的
                name="username"
                placeholder="请输入账号"
                // 将组件状态的值设置为表单的值
                // 此时value的值使用高阶组件传递来的值,不在使用组件状态的值
                value={values.username}
                // 监听表单输入的值设置为组件的状态
                // 将高阶组件的handleChange设置为表单元素的onChange,不在使用组件的函数了
                onChange={handleChange}
                // 监听表单的离焦事件,从高阶组件中获取
                onBlur={handleBlur}
              /> */}

              {/* 导入Field组件，替换input表单元素，去掉onChange，onBlur，value */}
              <Field
                className={styles.input}
                name="username"
                placeholder="请输入账号"
              />
            </div>
            {/* 长度为5到8位，只能出现数字、字母、下划线 */}
            {/* <div className={styles.error}>账号为必填项</div> */}
            {/* {errors.username && touched.username && <div className={styles.error}>{errors.username}</div>} */}

            {/* 导入 ErrorMessage 组件，替换原来的错误消息逻辑代码 */}
            <ErrorMessage
              className={styles.error}
              name="username"
              // 错误提示后被渲染成什么标签
              component="div"
            />

            <div className={styles.formItem}>
              {/* <input
                className={styles.input}
                // 用户名和密码监听使用的是一个函数,是通过表单的name属性来判断的
                name="password"
                type="password"
                placeholder="请输入密码"
                // 将组件状态的值设置为表单的值
                // 此时value的值使用高阶组件传递来的值,不在使用组件状态的值
                value={values.password}
                // 监听表单输入的值设置为组件的状态
                // 将高阶组件的handleChange设置为表单元素的onChange,不在使用组件的函数了
                onChange={handleChange}
                // 监听表单的离焦事件,从高阶组件中获取
                onBlur={handleBlur}
              /> */}

              {/* 导入Field组件，替换input表单元素，去掉onChange，onBlur，value */}
              <Field
                className={styles.input}
                name="password"
                type="password"
                placeholder="请输入密码"
              />
            </div>
            {/* 长度为5到12位，只能出现数字、字母、下划线 */}
            {/* <div className={styles.error}>账号为必填项</div> */}
            {/* {errors.password && touched.password && <div className={styles.error}>{errors.password}</div>} */}

            {/* 导入 ErrorMessage 组件，替换原来的错误消息逻辑代码 */}
            <ErrorMessage
              className={styles.error}
              name="password"
              // 错误提示后被渲染成什么标签
              component="div"
            />

            <div className={styles.formSubmit}>
              <button className={styles.submit} type="submit">
                登 录
              </button>
            </div>
          </Form>
          {/* </form> */}

          <Flex className={styles.backHome}>
            <Flex.Item>
              <Link to="/registe">还没有账号，去注册~</Link>
            </Flex.Item>
          </Flex>
        </WingBlank>
      </div >
    )
  }
}

// 使用 withFormik 高阶组件包装 Login 组件，为 Login 组件提供属性和方法
Login = withFormik({
  // 提供状态,相当于自己在组件写的状态 value={ username: '', password: '' }
  mapPropsToValues: () => ({ username: '', password: '' }),

  // 提交的表单事件
  handleSubmit: async (values, { props }) => {
    // 获取账号和密码
    const { username, password } = values;

    // 发送请求
    const { data: res } = await instance.post('/user/login', { username, password })
    // console.log("提交登录表单事件");

    // 从后台获取的数据解构出需要的值
    const { status, body, description } = res;

    if (status === 200) {
      // 登录成功,将数据存入到token中
      localStorage.setItem("login_Token", body.token);

      // 注意：无法在该方法中，通过 this 来获取到路由信息
      // 所以，需要通过 第二个对象参数中获取到 props 来使用 props
      // props.history.go(-1);

      /**
       * 修改登录成功跳转
       * 
       * 1.登陆成功后，判断是否需要跳转到用户想要访问的页面（判断props.location.state 是否有值）
       * 2.如果不需要，则直接调用history.go(-1) 返回上一页
       * 3.如果需要，就跳转到from.pathname 指定的页面（推荐使用replace方法模式，不是push）
       */
      if (!props.location.state) {
        // 表示直接进入到当前登录页面,不是访问了需要登录才能访问的页面而跳转到当前的页面中
        // [home,login,map]
        props.history.go(-1);
      } else {
        // 访问了需要登录才能访问的页面而跳转到当前的页面中,然后再访问跳转到需要访问的页面中
        // [home,login]  =>  [home,map]
        props.history.replace(props.location.state.from.pathname);
      }

    } else {
      // 登录失败
      Toast.info(description, 2, null, false)
    }
  },

  // 添加表单校验规则
  validationSchema: Yup.object().shape({
    // shape指定对象的结构
    username: Yup.string().required('账号为必填项').matches(REG_UNAME, '长度为5到8位,只能出现数字、字母、下划线'),
    password: Yup.string().required('密码为必填项').matches(REG_PWD, '长度为5到12位,只能出现数字、字母、下划线')
  })
})(Login)

export default Login;  //这里向外暴露的组件是被高阶组件包裹的Login组件


/**
 * formik来实现表单校验
 *
 * 1.安装： yarn add formik
 * 2.导入 withFormik，使用withFormit 高阶组件包裹Login组件
 * 3.为withFormit提供配置对象： mapPropsToValues / handleSubmit
 * 4.在Login组件中，通过props获取到values（表单元素值对象），handleSubmit，handleChange
 *    注意:在给表单设置handleChange事件时,要让其生效,要给表单添加name属性,并且name属性的值要和value的值相同
 * 5.使用values提供的值，设置为表单元素的value，使用handleChange设置为表单元素的onChange
 * 6.使用handleSubmit设置为表单的onSubmit
 * 7.在handleSubmit中，通过values获取到表单元素值
 * 8.在handleSubmit中，完成登录逻辑
 */


/**
 * 给登录功能添加表单验证
 *
 * 1.安装： yarn add yup，导入Yup
 * 2.在 withFormik 中添加配置项 validationSchema，使用 Yup 添加表单校验规则
 * 3.在 Login 组件中，通过 props 获取到 errors（错误信息）和  touched（是否访问过，注意：需要给表单元素添加 handleBlur 处理失焦点事件才生效！）
 * 4.在表单元素中通过这两个对象展示表单校验错误信
 */


/**
 * 组件简化处理
 *
 * 1.导入 Form组件，替换form元素，去掉onSubmit
 * 2.导入Field组件，替换input表单元素，去掉onChange，onBlur，value
 * 3.导入 ErrorMessage 组件，替换原来的错误消息逻辑代码
 * 4.去掉所有 props
 */


//  高阶组件是自己的薄弱点
